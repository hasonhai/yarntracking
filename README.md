## yarn tracking tool
To track how YARN using resource

Goals for tool:

- Portable
- Highly scalable
- Reliable
- Light

Current most compatible and setup notes:

- OS: CentOS 6.6
- Hadoop: HDP 2.4, admin HDFS user
- Slave nodes:
 - iotop
 - user with sudo permission
 - setup passwordless ssh for all nodes
- Master nodes:
 - user with sudo permission
 - setup passwordless ssh for all nodes

##FAQ

1. What is YARNProfiling?
This is a set of tools and scripts that help to understand the performance of application that running on YARN, especially MapReduce and Spark application.
We develop a distributed tool that run at the slave nodes to track how much CPU, memory, disk io, and network io a container actually uses (a container is a parallel task/executor in MapReduce of Spark jobs)

2. What can YARNProfiling answer?

- How one application actually uses cluster resource (which cannot been answer by other tools since they collect the info in term of whole node info)
- How one user uses the cluster resource (by collecting the resource usage from her applications)
- Where the bottleneck of the application is (by seeing how much resource the application use between cpu, memory, disk io, and network io usage)
- Which containers are the stragglers? Why does those containers took so long to finish?
- How is each node's performance? Is there any anomali at a specific node?
- How to "record" and "replay" a cluster operations so later we can suggest the optimization or tweaking the operation to measure the impact of the guess
...

3. What is the stage of the project?

4. How can I use the tool?

5. How can I support to develop the tool?
