#!/bin/bash
max_turn=$1	#required
waiting_time=$2 #required
inputdata=$3    #optional

if [ "$inputdata" = "" ]; then
  inputdata="logs/hadoop-hdfs-zkfc-svr01.spo.log"
fi
outputdata="outputWCauto"
exjarpath='/usr/hdp/2.3.4.0-3485/hadoop-mapreduce/hadoop-mapreduce-examples-*.jar'

for i in $( seq $max_turn ); do
  ( yarn jar $exjarpath wordcount $inputdata ${outputdata}${i}; hadoop fs -rm -r ${outputdata}${i} > /dev/null ) &
  # wait for continuous job submission or sleep for parallel jobs submission
  sleep $waiting_time
done
