#!/bin/bash
# Script to configure the parameters for yarntracking app

#################
if [ "$confdir" = "" ]; then #default check files in the same dir
  confdir=$( pwd  )
fi
trackers="$confdir/trackers.lst"
conffile="$confdir/yarntracking.conf"

if [ ! -f $trackers ]; then
  echo 'Cannot find list of trackers!'
  echo "Path was set at $trackers"
  exit 1 
fi

if [ ! -f $conffile ]; then
  echo 'Cannot find configuration for yarntracking app!'
  echo "Path was set at $conffile"
  exit 1
fi

################
#Assume observer directory is at $HOME. TODO: change this to make the app flexiple in location
observer_dir="${HOME}/observer"
tracker_dir="${HOME}/tracker"
export observer_dir="$observer_dir"
export tracker_dir="$tracker_dir"
################

#first export required parameters
user=$( grep -e "^user=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
access_key=$( grep -e "^access_key=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
if [ ! -f $confdir/$access_key ]; then
  echo "Cannot find access key to manage the nodes!"
  echo "Path was set at $confdir/$access_key"
  exit 1
fi
sudo_enabled=$( grep -e "^sudo_enabled=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
verbose=$( grep -e "^verbose=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
observer=$( grep -e "^observer=" $conffile | awk '{print $1}' | cut -d'=' -f2 )

export user="$user"
export access_key="$confdir/$access_key"
export sudo_enabled="$sudo_enabled"
export trackerlist="$trackers"
export observer="$observer"
export verbose="$verbose"

#handdle usage log directory
usage_log_dir=$( grep -e "^usage_log_dir=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
if [ "$usage_log_dir" = "" ]; then
  usage_log_dir="usagelog" #default value to store usage log"
fi

#if usage log dir start with / 
if [[ "${usage_log_dir:0:1}" == / ]]; then
  usage_log_dir="$usage_log_dir" #absolute path 
#if it start with ~ (home directory), convert to full path
elif [[ "${usage_log_dir:0:2}" == ~[/a-z] ]]; then
#else convert full path inside observer directory
  usage_log_dir=$( echo "$usage_log_dir" | sed "s/~/${HOME}/g" ) #relative path
else
  usage_log_dir="${observer_dir}/${usage_log_dir}" #relative path
fi
export usage_log_dir="$usage_log_dir"

#second export logging option
cpumem=$( grep -e "^cpumem=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
diskio=$( grep -e "^diskio=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
networkio=$( grep -e "^networkio=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
container_executed_log=$( grep -e "^container_executed_log=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
export cpumem="$cpumem"
export diskio="$diskio"
export networkio="$networkio"
export container_executed_log="$container_executed_log"

metadata_enabled=$( grep -e "^metadata_enabled=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
if [ "$metadata_enabled" = "" ]; then #check not empty, TODO: and not boolean value
  metadata_enabled="no" #default
fi
export metadata_enabled="$metadata_enabled"

#third export hadoop parameters
if [ "$container_executed_log" = "yes" ]; then
  hdfs_user=$( grep -e "^hdfs_user=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
  export hdfs_user=$hdfs_user
fi

kerberos=$( grep -e "^kerberos=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
if [ "$kerberos" = "yes" ]; then
  kerberos_keytab=$( grep -e "^kerberos_keytab=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
  kerberos_keytab="$confdir/$kerberos_keytab"
  if [ -e $kerberos_keytab ]; then
    kerberos_principal=$( grep -e "^kerberos_principal=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
    export kerberos="yes"
    export kerberos_keytab=$kerberos_keytab
    export kerberos_principal=$kerberos_principal
  else
    export kerberos="yes"
    export kerberos_principal="$user"
  fi
else
  export kerberos="no"
fi

timelineserver=$( grep -e "^timelineserver=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
timelineserverport=$( grep -e "^timelineserverport=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
resourcemanager=$( grep -e "^resourcemanager=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
resourcemanagerport=$( grep -e "^resourcemanagerport=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
historyserver=$( grep -e "^historyserver=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
historyserverport=$( grep -e "^historyserverport=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
sparkhistoryserver=$( grep -e "^sparkhistoryserver=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
sparkhistoryserverport=$( grep -e "^sparkhistoryserverport=" $conffile | awk '{print $1}' | cut -d'=' -f2 )

export timelineserver=$timelineserver
export timelineserverport=$timelineserverport
export resourcemanager=$resourcemanager
export resourcemanagerport=$resourcemanagerport
export historyserver=$historyserver
export historyserverport=$historyserverport
export sparkhistoryserver=$sparkhistoryserver
export sparkhistoryserverport=$sparkhistoryserverport	

#forth export the data store info
elasticsearch_enabled=$( grep -e "^elasticsearch_enabled=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
elasticsearch_host=$( grep -e "^elasticsearch_host=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
elasticsearch_port=$( grep -e "^elasticsearch_port=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
elasticsearch_index=$( grep -e "^elasticsearch_index=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
elasticsearch_type=$( grep -e "^elasticsearch_type=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
elasticsearch_daily_index=$( grep -e "^elasticsearch_daily_index=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
tar_enabled=$( grep -e "^tar_enabled=" $conffile | awk '{print $1}' | cut -d'=' -f2 )
duration=$( grep -e "^duration=" $conffile | awk '{print $1}' | cut -d'=' -f2 )

export elasticsearch_enabled=$elasticsearch_enabled
export elasticsearch_host=$elasticsearch_host
export elasticsearch_port=$elasticsearch_port
export elasticsearch_index=$elasticsearch_index
export elasticsearch_type=$elasticsearch_type
export elasticsearch_daily_index="$elasticsearch_daily_index"
export tar_enabled=$tar_enabled
export duration=$duration

#TODO: require function to check boolean, empty, number, string... to catch error

