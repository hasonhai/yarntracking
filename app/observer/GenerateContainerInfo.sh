#!/bin/bash

# input: appID
# output: list containers info stored in separated JSON files

if [ $verbose = "true" ]; then # for debugging
  debug="true"
else  
  debug="false"
fi

function die {
  timestamp=$( date )
  echo "$timestamp ERROR: $1"
  exit 1
}

if [ -z "$1" ]; then
  die "Please specify the application id"
fi
appId=$1
Id=$( echo $appId | awk 'BEGIN { FS = "_"; OFS = "_" } {print $2,$3}' )
jobId="job_$Id"

echo "Receive app with id $Id to process..."

TrackAppDir="$observerdir"
if [ ! -z "$2" ]; then
  OutputDir="$2"
else
  OutputDir="${TrackAppDir}/output" #default is inside working directory
fi
if [ ! -e "$OutputDir" ]; then
  mkdir -p $OutputDir || die "Output directory for all app is not existed" >> $observerlogfile
fi

if [ "$kerberos" = "yes" ]; then
  NEGOTIATE_PAM="-i --negotiate -u:"
else
  NEGOTIATE_PAM=""
fi

# Should read this from Hadoop configuration (/etc/hadoop/conf)
TimelineServer="$timelineserver" #Be careful with Resource Manager HA
TSPort="$timelineserverport"
ResourceManager="$resourcemanager"
RMPort="$resourcemanagerport"
HistoryServer="$historyserver"
HSPort="$historyserverport"

HDFSadmin="$hdfs_user" #This admin user is needed to get logs from other user space
LoadDir="$usage_log_dir"

# Create temporary folder to process data, and output folder to store .jsonprofile files
appIdDir=${OutputDir}/${appId}
appIdJsonDir=${OutputDir}/${appId}_json
if [ ! -e $appIdJsonDir ]; then
  mkdir -p $appIdJsonDir || die "Cannot create output directory for $appId" >> $observerlogfile
fi

echo "Get application info..."
appInfo=$( curl -s $NEGOTIATE_PAM http://${TimelineServer}:${TSPort}/ws/v1/applicationhistory/apps/${appId} | tail -n 1 )

if [ "$debug" = "true" ]; then echo "appinfo=$appInfo"; fi
# parsing app info, the format of json maybe different during runtime or with different schedulers
if [ "$appInfo" != "" ]; then

  appInfo_tmp=$( echo -e "$appInfo" | sed -e 's/[{}]//g' | sed 's/,"/\n"/g' )
  currentAppAttemptId=$( echo -e "$appInfo_tmp" | grep "\"currentAppAttemptId\":" | cut -d':' -f2- | sed 's/"//g' )
  appOwner=$( echo -e "$appInfo_tmp" | grep "\"user\":" | cut -d':' -f2- | sed 's/"//g' )
  appName=$( echo -e "$appInfo_tmp" | grep "\"name\":" | cut -d':' -f2- | sed 's/"//g' )
  queue=$( echo -e "$appInfo_tmp" | grep "\"queue\":" | cut -d':' -f2- | sed 's/"//g' )
  appType=$( echo -e "$appInfo_tmp" | grep "\"type\":" | cut -d':' -f2- | sed 's/"//g' )
  appState=$( echo -e "$appInfo_tmp" | grep "\"appState\":" | cut -d':' -f2- | sed 's/"//g' )
  runningContainers=$( echo -e "$appInfo_tmp" | grep "\"runningContainers\":" | cut -d':' -f2- )
  apptrackingUrl=$( echo -e "$appInfo_tmp" | grep "\"trackingUrl\":" | cut -d':' -f2- | sed 's/"//g' )
  finalAppStatus=$( echo -e "$appInfo_tmp" | grep "\"finalAppStatus\":" | cut -d':' -f2- | sed 's/"//g' )
  appsubmittedTime=$( echo -e "$appInfo_tmp" | grep "\"submittedTime\":" | cut -d':' -f2- )
  appstartedTime=$( echo -e "$appInfo_tmp" | grep "\"startedTime\":" | cut -d':' -f2- )
  appfinishedTime=$( echo -e "$appInfo_tmp" | grep "\"finishedTime\":" | cut -d':' -f2- )
  appelapsedTime=$( echo -e "$appInfo_tmp" | grep "\"elapsedTime\":" | cut -d':' -f2- )
  
else
  if [ "$debug" = "true" ]; then 
    echo "Response from timeline server: $appInfo" >> $observerlogfile
  fi
  die "Cannot get app info from timeline server for app $appId" >> $observerlogfile
fi

if [ "$debug" = "true" ]; then
  echo "currentAppAttemptId=$currentAppAttemptId"
  echo "appOwner=$appOwner"
  echo "appName=$appName"
  echo "queue=$queue"
  echo "appType=$appType"
  echo "appState=$appState"
  echo "runningContainers=$runningContainers"
  echo "apptrackingUrl=$apptrackingUrl"
  echo "finalAppStatus=$finalAppStatus"
  echo "appsubmittedTime=$appsubmittedTime"
  echo "appstartedTime=$appstartedTime"
  echo "appfinishedTime=$appfinishedTime"
  echo "appelapsedTime=$appelapsedTime"
fi

if [ "$container_executed_log" = "yes" ]; then
  # get logs of containers, create a directory for the application
  echo "Get logs of containers..."
  ${TrackAppDir}/getlogs.sh $appId $appOwner $HDFSadmin $appIdDir
  #TODO: Convert log records to JSON record and later combine to container JSON file
fi

# get list of attemts, for each attempt, find list of containers associate with the attempt
echo "Get list of app attempts..."
appAttemptinfo=$( curl -s $NEGOTIATE_PAM "http://${TimelineServer}:${TSPort}/ws/v1/applicationhistory/apps/${appId}/appattempts" | tail -n 1 )

appAttemptList=$( echo "$appAttemptinfo" | python ${TrackAppDir}/getattemptlist.py )
# If we cannot get the list of container from timeline server, we have to figure it out from diffreent resources.
# This makes the app cannot recornise which containers belong to which attempt.
NoAttempt=0 #default value
if [ "$appAttemptList" != "" ]; then
  echo -e "Found attempt list:\n$appAttemptList"
  NoAttempt="$( echo "$appAttemptList" | wc -w )"
  if [ "$debug" = "true" ]; then echo "NoAttempt=$NoAttempt"; fi
  if [ $NoAttempt -eq 0 ]; then
    echo "Cannot get application attempts list for app $appId" >> $observerlogfile
    exit 1 # non successful code
  elif [ $NoAttempt -gt 1 ]; then
    echo "Application $appId has more than 1 attempts"
    #check if we can get the container list from the timeline server
    appAttemptId_test=$( echo -e "$appAttemptList" | tail -n 1 ) # get the last one to test
    containerlist_info=$( curl -s $NEGOTIATE_PAM "http://${TimelineServer}:${TSPort}/ws/v1/applicationhistory/apps/${appId}/appattempts/${appAttemptId_test}/containers" | tail -n 1 )
    containerlist=$( echo "$containerlist_info" | python ${TrackAppDir}/getcontainerlist2.py )
    if [ $( echo "$containerlist" | wc -w ) -gt 1  ]; then #TS returns many container info
      echo "We got container list directly from timeline server for app attempt ${appAttemptId_test}"
      containerlist="" #reset value for later use
    else
      echo "We could not get container list directly from timeline server for app attempt ${appAttemptId_test}"
      exit 1
    fi
  fi
fi
# only one attempt found from this point
for appAttemptId in $appAttemptList; do #there should be only one attempt Id
  appAttemptInfo=$( curl -s $NEGOTIATE_PAM "http://${TimelineServer}:${TSPort}/ws/v1/applicationhistory/apps/${appId}/appattempts/${appAttemptId}" | tail -n 1 )
  if [ "$debug" = "true" ]; then echo "appAttemptInfo=$appAttemptInfo"; fi
  # parsing app attempt info
  if [ "$appAttemptInfo" != "" ]; then
    attemptinfo_tmp=$( echo -e "$appAttemptInfo" | sed -e 's/[{}]//g' | sed 's/,"/\n"/g' )
    host=$( echo -e "$attemptinfo_tmp" | grep "\"host\":" | cut -d':' -f2- | sed 's/"//g' )
    rpcPort=$( echo -e "$attemptinfo_tmp" | grep "\"rpcPort\":" | cut -d':' -f2- )
    trackingUrl=$( echo -e "$attemptinfo_tmp" | grep "\"trackingUrl\":" | cut -d':' -f2- | sed 's/"//g' )
    originalTrackingUrl=$( echo -e "$attemptinfo_tmp" | grep "\"originalTrackingUrl\":" | cut -d':' -f2- | sed 's/"//g' )
    diagnosticsInfo=$( echo -e "$attemptinfo_tmp" | grep "\"diagnosticsInfo\":" | cut -d':' -f2- | sed 's/"//g' )
    appAttemptState=$( echo -e "$attemptinfo_tmp" | grep "\"appAttemptState\":" | cut -d':' -f2- | sed 's/"//g' )
    amContainerId=$( echo -e "$attemptinfo_tmp" | grep "\"amContainerId\":" | cut -d':' -f2- | sed 's/"//g' )
    startedTime=$( echo -e "$attemptinfo_tmp" | grep "\"startedTime\":" | cut -d':' -f2- )
    finishedTime=$( echo -e "$attemptinfo_tmp" | grep "\"finishedTime\":" | cut -d':' -f2- )
  else
    echo "cannot get app attempt info from timeline server"
    exit 1
  fi

  if [ "$debug" = "true" ]; then
    echo "host=$host"
    echo "rpcPort=$rpcPort"
    echo "trackingUrl=$trackingUrl"
    echo "originalTrackingUrl=$originalTrackingUrl"
    echo "diagnosticsInfo=$diagnosticsInfo"
    echo "appAttemptState=$appAttemptState"
    echo "amContainerId=$amContainerId"
    echo "startedTime=$startedTime"
    echo "finishedTime=$finishedTime"
  fi

  # get data locality info
  data_locality="$( ${TrackAppDir}/getDataLocalityForAppAttempt.sh $appAttemptId $ResourceManager $RMPort ${appId} )"
  if [ "$debug" = "true" ]; then echo "data_locality=$data_locality"; fi
  if [ "$( echo  "$data_locality" | wc -w )" = "7" ]; then
    AllocatedContainer=$( echo "$data_locality" | awk '{print $1}' )
    LocalRequest_LocalSatisfy=$( echo "$data_locality" | awk '{print $2}' )
    RackRequest_LocalSatisfy=$( echo "$data_locality" | awk '{print $3}' )
    RackRequest_RackSatisfy=$( echo "$data_locality" | awk '{print $4}' )
    OffSwitchRequest_LocalSatisfy=$( echo "$data_locality" | awk '{print $5}' )
    OffSwitchRequest_RackSatisfy=$( echo "$data_locality" | awk '{print $6}' )
    OffSwitchRequest_OffSwitchSatisfy=$( echo "$data_locality" | awk '{print $7}' )
  else
    AllocatedContainer="null"
    LocalRequest_LocalSatisfy="null"
    RackRequest_LocalSatisfy="null"
    RackRequest_RackSatisfy="null"
    OffSwitchRequest_LocalSatisfy="null"
    OffSwitchRequest_RackSatisfy="null"
    OffSwitchRequest_OffSwitchSatisfy="null"
  fi

  if [ "$debug" = "true" ]; then
    echo "AllocatedContainer=$AllocatedContainer"
    echo "LocalRequest_LocalSatisfy=$LocalRequest_LocalSatisfy"
    echo "RackRequest_LocalSatisfy=$RackRequest_LocalSatisfy"
    echo "RackRequest_RackSatisfy=$RackRequest_RackSatisfy"
    echo "OffSwitchRequest_LocalSatisfy=$OffSwitchRequest_LocalSatisfy"
    echo "OffSwitchRequest_RackSatisfy=$OffSwitchRequest_RackSatisfy"
    echo "OffSwitchRequest_OffSwitchSatisfy=$OffSwitchRequest_OffSwitchSatisfy"
  fi

  #TODO: get list of containers for that appapptemp from resource manager
  #first way using python
  #containerlist=$( python ${TrackAppDir}/getcontainerlist.py ${TimelineServer} ${TSPort} ${appId} ${appAttemptId} )
  #second way
  containerlist_info=$( curl -s $NEGOTIATE_PAM "http://${TimelineServer}:${TSPort}/ws/v1/applicationhistory/apps/${appId}/appattempts/${appAttemptId}/containers" | tail -n 1 )
  if [ "$debug" = "true" ]; then echo "$containerlist_info" >> $observerlogfile ; fi
  containerlist2=$( echo "$containerlist_info" | python ${TrackAppDir}/getcontainerlist2.py )
  if [ "$containerlist" = "" ]; then containerlist=$containerlist2; fi # switch to second method
  containerlist_save=$containerlist2
  #TODO: parse the containerlist_JSON using the python script running on tracker node (they have similar structure)
  if [ $( echo $containerlist | wc -w ) -gt 1  ]; then #TS returns many container info
    echo "We got container list directly from timeline server"
    datafrom="Timeline Server"
    if [ "$appType" = "MAPREDUCE" ]; then    
      python ${TrackAppDir}/gettasklist.py ${HistoryServer} ${HSPort} ${jobId} > ${TrackAppDir}/match_file_$appId #for later use
    else
      echo "This is not a MAPREDUCE jobs. We do not need to match taskid with containerid"
    fi
  else
    if [ "$appType" = "MAPREDUCE" ]; then
      #get task list and map to container, remember the app master container
      echo "We have to query container list from mapreduce history server: server ${HistoryServer} port ${HSPort} id ${jobId}"
      python ${TrackAppDir}/gettasklist.py ${HistoryServer} ${HSPort} ${jobId} > ${TrackAppDir}/match_file_$appId
      #should check if "match_file_$appId" is not empty => that means we cannot get the container list from HS
      if [ $( cat ${TrackAppDir}/match_file_$appId | wc -l ) -gt 0 ]; then
        echo "Container list from History Server is good to use"
        if [ $( echo $containerlist | wc -w ) -eq 1  ]; then #TS only return one container
          containerlist="$containerlist $( cat ${TrackAppDir}/match_file_$appId | awk {'print $2'} )"
        else #TS did not return any container
          containerlist="$amContainerId $( cat ${TrackAppDir}/match_file_$appId | awk {'print $2'} )"
        fi
        datafrom="MapReduce History Server"
      else
        echo "Cannot use the info from History Server, switch to use other sources"
        containerlist="" # to switch the app to use the below code segment
      fi
    else
      containerlist="" # to switch the app to use the below code segment
    fi
    if [ "$containerlist" = "" ]; then #is MR but cannot get container list from HS, or is other kind of job
      if [ -f ${appIdDir}/containers ]; then # 1st way: get container list from aggregated log
        # best effort way because container may belong to many app attempt, or node cannot send log to aggregated log service
        containerlist=$( awk '{print $1}' ${appIdDir}/containers )
        datafrom="HDFS appplication aggregated logs"
        #TODO: Remember to remove this directory after use
      elif [ -d $LoadDir ]; then # 2nd way: get container list from avaialble loadlog in $LoadDir
        # best effort way because container may belong to many app attempt, or node cannot send logload usage to the collector
        appIdNumber=$( echo "$appId" | cut -d'_' -f2,3 --output-delimiter="_" )
        containerlist=$( ls -lh $LoadDir | grep "$appIdNumber" | awk '{print $9}' | cut -d'.' -f1 | uniq )
        datafrom="YarnTracking Resource Usage Logs"
      else
        containerlist=""
        datafrom="nowhere"
        echo "Cannot get containers list for app $appId" >> $observerlogfile
        die "Cannot get container list"
      fi
    fi      
  fi

  if [ "$debug" = "true" ]; then
    echo -e "Timeline Server returns $( echo "$containerlist_save" | wc -w  ) containers for app $appId" >> $observerlogfile
    echo -e "App $appId is $appType type, we get and use $( echo $containerlist | wc -w ) containers from $datafrom:\n$containerlist" >> $observerlogfile
  fi
 
  for container_id in $containerlist; do
    echo -n "Processing ${container_id}..."
    #get container info
    container_info="$( curl -s $NEGOTIATE_PAM "http://${TimelineServer}:${TSPort}/ws/v1/applicationhistory/apps/${appId}/appattempts/${appAttemptId}/containers/${container_id}" | tail -n 1 )"
    special_container="true"    
    echo container_info=$container_info
    # =============
    task_type="" #in case of Application master
    if [ "$container_info" != "" ]; then #TS returns container info
      if [ $( echo "$container_info" | grep -c "ContainerNotFoundException" ) -eq 1 ]; then
        #get information from task info
        echo "Cannot get container metadata from timeline service, have it been enabled? https://issues.apache.org/jira/browse/YARN-3978"
        if [ "$appType" = "MAPREDUCE" ]; then #for MAPREDUCE jobs
          taskid=$( cat ${TrackAppDir}/match_file_${appId} | grep "$container_id" | awk {'print $1'} ) #assume container runs 1 task only
          taskinfo=$( curl -s $NEGOTIATE_PAM "http://${HistoryServer}:${HSPort}/ws/v1/history/mapreduce/jobs/${jobId}/tasks/${taskid}" | tail -n 1 )
        
          task_type=$( echo "$taskinfo" | cut -d',' -f7 | cut -d':' -f2- | sed 's/"//g' )
          task_starttime=$( echo "$taskinfo" | cut -d',' -f1 | cut -d':' -f3- )
          task_endtime=$( echo "$taskinfo" | cut -d',' -f2 | cut -d':' -f2- )
          container_starttime=$task_starttime
          container_endtime=$task_endtime #TODO: this causes bug when task fails repeatedly
          container_elapsedtime=$( echo "$taskinfo" | cut -d',' -f3 | cut -d':' -f2- )
          container_finalstate=$( echo "$taskinfo" | cut -d',' -f6 | cut -d':' -f2- | sed 's/"//g' )
          container_exitCode=0
          container_diagnostics="null"
        elif [ "$appType" = "SPARK" ]; then # for other jobs, such as spark
          container_starttime=$appstartedTime
          container_endtime=$appfinishedTime
          container_elapsedtime=$appelapsedTime
          container_finalstate="null"
          container_exitCode=0
          container_diagnostics="null"
        else # TEZ jobs and others
          container_starttime=0
          container_endtime=0
          container_elapsedtime=0
          container_finalstate="null"
          container_exitCode=0
          container_diagnostics="null"        
        fi
      elif [ $( echo "$container_info" | grep -c "containerId" ) -eq 1  ]; then
        info_tmp=$( echo -e "$container_info" | sed -e 's/[{}]//g' | sed 's/,"/\n"/g' )
        container_starttime=$( echo -e "$info_tmp" | grep "\"startedTime\":" | cut -d':' -f2- )
        container_endtime=$( echo -e "$info_tmp" | grep "\"finishedTime\":" | cut -d':' -f2- )
        container_elapsedtime=$( echo -e "$info_tmp" | grep "\"elapsedTime\":" | cut -d':' -f2- )
        container_finalstate=$( echo -e "$info_tmp" | grep "\"containerState\":" | cut -d':' -f2- | sed 's/"//g' )
        container_exitCode=$( echo -e "$info_tmp" | grep "\"containerExitStatus\":" | cut -d':' -f2- )
        container_diagnostics=$( echo -e "$info_tmp" | grep "\"diagnosticsInfo\":" | cut -d':' -f2- | sed 's/"//g' )
        containerLogsLink=$( echo -e "$info_tmp" | grep "\"logUrl\":" | cut -d':' -f2- | sed 's/"//g' )
        MemoryRequestedMB=$( echo -e "$info_tmp" | grep "\"allocatedMB\":" | cut -d':' -f2- )
        MemoryAssignedMB=$( echo -e "$info_tmp" | grep "\"allocatedMB\":" | cut -d':' -f2- )
        VCoresAssigned=$( echo -e "$info_tmp" | grep "\"allocatedVCores\":" | cut -d':' -f2- )
        VCoresRequested=$( echo -e "$info_tmp" | grep "\"allocatedVCores\":" | cut -d':' -f2- )
        priority=$( echo -e "$info_tmp" | grep "\"priority\":" | cut -d':' -f2- )
        assignedNodeId=$( echo -e "$info_tmp" | grep "\"assignedNodeId\":" | cut -d':' -f2- | sed 's/"//g' )
        assignedNode=$( echo "$assignedNodeId" | cut -d':' -f1 )
        nodeHttpAddress=$( echo -e "$info_tmp" | grep "\"nodeHttpAddress\":" | cut -d':' -f2- | sed 's/"//g' )
        if [ "$appType" = "MAPREDUCE"  ]; then
          taskid=$( cat ${TrackAppDir}/match_file_${appId} | grep "$container_id" | awk {'print $1'} ) #assume container runs 1 task only
          taskinfo=$( curl -s $NEGOTIATE_PAM "http://${HistoryServer}:${HSPort}/ws/v1/history/mapreduce/jobs/${jobId}/tasks/${taskid}" | tail -n 1 )
          task_type=$( echo "$taskinfo" | cut -d',' -f7 | cut -d':' -f2- | sed 's/"//g' )
          task_starttime=$( echo "$taskinfo" | cut -d',' -f1 | cut -d':' -f3- )
          task_endtime=$( echo "$taskinfo" | cut -d',' -f2 | cut -d':' -f2- )
        fi
      else # in case timeline server return incorrect format
        echo "Cannot figure out the format: $container_info" >> $observerlogfile
      fi
    else #timeline server does not response or return null
      container_starttime=0       #TODO
      container_endtime=0         #TODO
      container_elapsedtime=0     #TODO
      container_finalstate="null" #TODO
      container_exitCode=0        #TODO
      container_diagnostics=""    #TODO
      if [ "$appType" = "MAPREDUCE"  ]; then
        taskid=$( cat ${TrackAppDir}/match_file_${appId} | grep "$container_id" | awk {'print $1'} ) #assume container runs 1 task only
        taskinfo=$( curl -s $NEGOTIATE_PAM "http://${HistoryServer}:${HSPort}/ws/v1/history/mapreduce/jobs/${jobId}/tasks/${taskid}" | tail -n 1 )
        task_type=$( echo "$taskinfo" | cut -d',' -f7 | cut -d':' -f2- | sed 's/"//g' )
        task_starttime=$( echo "$taskinfo" | cut -d',' -f1 | cut -d':' -f3- )
        task_endtime=$( echo "$taskinfo" | cut -d',' -f2 | cut -d':' -f2- )
        container_starttime=$task_starttime
        container_endtime=$task_endtime
        let "container_elapsedtime=$container_endtime-$container_starttime" 
      fi
      if [ -f ${LoadDir}/${container_id}.info ]; then
        containerLogsLink=$( cat ${LoadDir}/${container_id}.info | cut -d',' -f8 | grep "containerLogsLink" | cut -d':' -f2- | sed 's/"//g' )
        MemoryRequestedMB=$( cat ${LoadDir}/${container_id}.info | cut -d',' -f6 | grep "totalMemoryNeededMB" | cut -d':' -f2- )
        VCoresRequested=$( cat ${LoadDir}/${container_id}.info | cut -d',' -f7 | grep "totalVCoresNeeded" | cut -d':' -f2- )
        MemoryAssignedMB=$( cat ${LoadDir}/${container_id}.info | cut -d',' -f6 | grep "totalMemoryNeededMB" | cut -d':' -f2- )
        VCoresAssigned=$( cat ${LoadDir}/${container_id}.info | cut -d',' -f7 |  grep "totalVCoresNeeded" | cut -d':' -f2- )
      else
        containerLogsLink="null"
        MemoryRequestedMB=0
        VCoresRequested=0
        MemoryAssignedMB=0
        VCoresAssigned=0
      fi
    fi    

    if [ "$debug" = "true" ]; then    
      echo "container_starttime=$container_starttime"
      echo "container_endtime=$container_endtime"
      echo "container_elapsedtime=$container_elapsedtime"
      echo "container_finalstate=$container_finalstate"
      echo "container_exitCode=$container_exitCode"
      echo "container_diagnostics=$container_diagnostics"
      echo "containerLogsLink=$containerLogsLink"
      echo "MemoryRequestedMB=$MemoryRequestedMB"
      echo "VCoresRequested=$VCoresRequested"
      echo "MemoryAssignedMB=$MemoryAssignedMB"
      echo "VCoresAssigned=$VCoresAssigned"
    fi
    
    #Parse node info here
    nodeinfojsonfile="${LoadDir}/${container_id}.nodeinfo"
    if [ ! -e $nodeinfojsonfile ]; then
      echo "$nodeinfojsonfile is not existed, use default file instead" >> $observerlogfile
      if [ "$assignedNode" != ""  ]; then
        nodeinfodefault="${LoadDir}/${assignedNode}.nodeinfo"
        if [ -e $nodeinfodefault ]; then
          echo "$nodeinfodefault is used for nodeinfo" >> $observerlogfile
          nodeinfotouse=$nodeinfodefault
          if [ "$debug" = "true" ]; then
            echo "Container ${container_id} has task info: $taskinfo" >> $observerlogfile
            echo "Container ${container_id} has container info: $container_info" >> $observerlogfile
          fi
        else
          echo "$nodeinfodefault is also not existed" >> $observerlogfile
        fi
      fi
    else
      nodeinfotouse=$nodeinfojsonfile        
    fi

    #TODO: Some container are really weird
    if [ "$appType" = "MAPREDUCE" ]; then
      if [ "$taskid" = "" ]; then
        if [ "$container_exitCode" = "-1000" ]; then
          if [ "$container_endtime" = "0" ]; then
            special_container="true" #These kind of container is not realy executed
          fi
        elif [ "$container_exitCode" = "-100" ]; then
          if [ $container_elapsedtime -lt 3000 ]; then # shorter than 3s
            if [ ! -e ${LoadDir}/${container_id}.loadlog ]; then
              special_container="true"
            fi
          fi
        else
          special_container="false"
        fi
      fi
    fi
    
    #TODO: parse loadlog to JSON format
    if [ -e ${LoadDir}/${container_id}.loadlog ]; then
      CPUload=$( cat ${LoadDir}/${container_id}.loadlog | python ${TrackAppDir}/convertCPULoadtoJSON.py | sed 's/.$//' ) # sed to get rid of the last comma
      Memoryload=$( cat ${LoadDir}/${container_id}.loadlog | python ${TrackAppDir}/convertMemoryUsedtoJSON.py | sed 's/.$//' )
    else
      CPUload=""
      Memoryload=""
      echo "WARN: CPU and Memory usage log of container ${container_id} is not found" >> $observerlogfile
    fi

    # Process disk IO
    if [ "$diskio" = "yes"  ]; then
      if [ -e ${LoadDir}/${container_id}.disklog ]; then
        Diskload=$( cat ${LoadDir}/${container_id}.disklog | python ${TrackAppDir}/convertDiskUsedtoJSON.py | sed 's/.$//' )
      else
        Diskload=""
        echo "WARN: Disk usage log of container ${container_id} is not found" >> $observerlogfile
      fi
    else
      Diskload=""      
    fi

    # Process network IO
    if [ "$networkio" = "yes"  ]; then
      if [ -e ${LoadDir}/${container_id}.netlog ]; then
        Networkload=$( cat ${LoadDir}/${container_id}.netlog | python ${TrackAppDir}/convertDiskUsedtoJSON.py | sed 's/.$//' )
      else
        Networkload=""
        echo "WARN: Network usage log of container ${container_id} is not found" >> $observerlogfile
      fi
    else
      Networkload=""
    fi
   
    #TODO: Doing some statistic
    actualMemoryUseMinMB="null"
    actualMemoryUseMaxMB="null"
    actualMemoryUseAvgMB="null"
    actualAvgLoad="null"
    #TODO: Enriching the info
    container_data_source="null" #link to input split, may help to get input size
    data_locality_level="null" #for this container, 1: local data, 2: same rack data, 3: off rack data
    priority="null"
    container_task_type="null" #(AM, mapper, reducer, executor, driver, unknown),
    #TODO: combine all info into one JSON record
    outputjson="$appIdJsonDir/${container_id}.jsonprofile"
    echo -n "{" > $outputjson
    echo -n "\"container_id\":\"${container_id}\"," >> $outputjson
    echo -n "\"container_starttime\":${container_starttime}," >> $outputjson
    echo -n "\"container_endtime\":${container_endtime}," >> $outputjson
    echo -n "\"container_elapsedtime\":${container_elapsedtime}," >> $outputjson
    echo -n "\"container_finalstate\":\"${container_finalstate}\"," >> $outputjson
    echo -n "\"exitCode\":${container_exitCode}," >> $outputjson
    echo -n "\"diagnostics\":\"${container_diagnostics}\"," >> $outputjson
    echo -n "\"MemoryRequestedMB\":${MemoryRequestedMB}," >> $outputjson
    echo -n "\"VCoresRequested\":${VCoresRequested}," >> $outputjson
    echo -n "\"MemoryAssignedMB\":${MemoryAssignedMB}," >> $outputjson
    echo -n "\"VCoresAssigned\":${VCoresAssigned}," >> $outputjson
    echo -n "\"actualMemoryUse\":" >> $outputjson
    echo -n "{" >> $outputjson
    echo -n "\"actualMemoryUseMinMB\":\"${actualMemoryUseMinMB}\"," >> $outputjson
    echo -n "\"actualMemoryUseMaxMB\":\"${actualMemoryUseMaxMB}\"," >> $outputjson
    echo -n "\"actualMemoryUseAvgMB\":\"${actualMemoryUseAvgMB}\"" >> $outputjson
    echo -n "}," >> $outputjson
    echo -n "\"actualAvgLoad\":\"${actualAvgLoad}\"," >> $outputjson
    echo -n "\"container_data_source\":\"${container_data_source}\"," >> $outputjson #link to input split, may help to get input size
    echo -n "\"data_locality_level\":\"${data_locality_level}\"," >> $outputjson #for this container
    echo -n "\"priority\":\"${priority}\"," >> $outputjson
    # Special treatment for mapreduce
    echo -n "\"container_tasks\":" >> $outputjson
    echo -n "[" >> $outputjson
    if [ "${appType}" = "MAPREDUCE" ]; then #temporary works with 1 task/container
      echo -n "{" >> $outputjson
      if [ "${taskid}" = "" ]; then #AM
        if [ "$special_container" = "true" ]; then
          echo -n "\"task_id\":\"null\"," >> $outputjson
          echo -n "\"task_type\":\"None\"," >> $outputjson #(AM, mapper, reducer)
          echo -n "\"task_starttime\":0," >> $outputjson
          echo -n "\"task_endtime\":0," >> $outputjson
          echo -n "\"counter\":{}" >> $outputjson
        elif [ "${container_id}" = "$amContainerId"  ]; then
          echo -n "\"task_id\":\"null\"," >> $outputjson
          echo -n "\"task_type\":\"AM\"," >> $outputjson
          echo -n "\"task_starttime\":$container_starttime," >> $outputjson
          echo -n "\"task_endtime\":$container_endtime," >> $outputjson
          echo -n "\"counter\":{}" >> $outputjson
        else
          echo -n "\"task_id\":\"null\"," >> $outputjson
          echo -n "\"task_type\":\"None\"," >> $outputjson
          echo -n "\"task_starttime\":0," >> $outputjson
          echo -n "\"task_endtime\":0," >> $outputjson
          echo -n "\"counter\":{}" >> $outputjson
        fi
      else
        echo -n "\"task_id\":\"$taskid\"," >> $outputjson
        echo -n "\"task_type\":\"$task_type\"," >> $outputjson #(AM, mapper, reducer)
        echo -n "\"task_starttime\":$task_starttime," >> $outputjson
        echo -n "\"task_endtime\":$task_endtime", >> $outputjson
        echo -n "\"counter\":" >> $outputjson        
        counterinfo=$( python ${TrackAppDir}/gettaskcounter.py $HistoryServer $HSPort $jobId $taskid )
        if [ "$counterinfo" != ""  ]; then
           echo -n "$counterinfo" >> $outputjson
        else
           echo -n "{}" >> $outputjson
        fi
      fi
      echo -n "}" >> $outputjson
    else
      if [ "${container_id}" = "$amContainerId"  ]; then
          echo -n "{" >> $outputjson
          echo -n "\"task_id\":\"null\"," >> $outputjson
          echo -n "\"task_type\":\"AM\"," >> $outputjson
          echo -n "\"task_starttime\":$container_starttime," >> $outputjson
          echo -n "\"task_endtime\":$container_endtime," >> $outputjson
          echo -n "\"counter\":{}" >> $outputjson
          echo -n "}" >> $outputjson
      fi
    fi
    echo -n "]," >> $outputjson
    echo -n "\"containerLogsLink\":\"${containerLogsLink}\"," >> $outputjson
    # Application info
    echo -n "\"application_info\":" >> $outputjson
    echo -n "{" >> $outputjson
    echo -n "\"appId\":\"$appId\"," >> $outputjson
    echo -n "\"appAttemptList\":" >> $outputjson
    echo -n "[" >> $outputjson
    # ====== convert apptempt list to json
    apptemptjson=""
    for appattempt in $appAttemptList; do
      apptemptjson="$apptemptjson,{\"appAttemptId\":\"$appattempt\"}"
    done
    apptemptjson=$( echo "$apptemptjson" | sed 's/^.\{1\}//' )
    # ======= end of converting
    echo -n $apptemptjson >> $outputjson
    echo -n "]," >> $outputjson
    echo -n "\"lastAppAttemptId\":\"$currentAppAttemptId\"," >> $outputjson #because we extract info after app finish
    echo -n "\"appOwner\":\"$appOwner\"," >> $outputjson
    echo -n "\"appName\":\"$appName\"," >> $outputjson
    echo -n "\"queue\":\"$queue\"," >> $outputjson #be careful with this, capacity scheduler has queues, others may not
    echo -n "\"appType\":\"$appType\"," >> $outputjson #MAPREDUCE, SPARK, TEZ,...
#   echo -n "\"host\":\"$apphost\"," >> $outputjson #info duplicate in app attemp info
#   echo -n "\"rpcPort\":$apprpcPort," >> $outputjson #info duplicate in app attemp info
#   echo -n "\"currentAppState\":\"$appState\"," >> $outputjson #because we extract info after app finish
    echo -n "\"runningContainers\":$runningContainers," >> $outputjson
    echo -n "\"trackingUrl\":\"$apptrackingUrl\"," >> $outputjson
    echo -n "\"finalAppStatus\":\"$finalAppStatus\"," >> $outputjson
    echo -n "\"submittedTime\":$appsubmittedTime," >> $outputjson
    echo -n "\"startedTime\":$appstartedTime," >> $outputjson
    echo -n "\"finishedTime\":$appfinishedTime," >> $outputjson
    echo -n "\"elapsedTime\":$appelapsedTime" >> $outputjson
    echo -n "}," >> $outputjson
    # App Attempt Info
    echo -n "\"appAttempts\":" >> $outputjson
    echo -n "{" >> $outputjson
    echo -n "\"appAttemptId\":\"$appAttemptId\"," >> $outputjson
    echo -n "\"host\":\"$host\"," >> $outputjson
    echo -n "\"rpcPort\":$rpcPort," >> $outputjson
    echo -n "\"trackingUrl\":\"$trackingUrl\"," >> $outputjson
    echo -n "\"originalTrackingUrl\":\"$originalTrackingUrl\"," >> $outputjson
    echo -n "\"diagnosticsInfo\":\"$diagnosticsInfo\"," >> $outputjson
    echo -n "\"appAttemptState\":\"$appAttemptState\"," >> $outputjson
    echo -n "\"amContainerId\":\"$amContainerId\"," >> $outputjson
    echo -n "\"startedTime\":$startedTime," >> $outputjson
    echo -n "\"finishedTime\":$finishedTime," >> $outputjson #what if app attempt not finish?
    echo -n "\"AllocatedContainers\":$AllocatedContainer," >> $outputjson
    echo -n "\"LocalRequest_LocalSatisfy\":$LocalRequest_LocalSatisfy," >> $outputjson
    echo -n "\"RackRequest_LocalSatisfy\":$RackRequest_LocalSatisfy," >> $outputjson
    echo -n "\"RackRequest_RackSatisfy\":$RackRequest_RackSatisfy," >> $outputjson
    echo -n "\"OffSwitchRequest_LocalSatisfy\":$OffSwitchRequest_LocalSatisfy," >> $outputjson
    echo -n "\"OffSwitchRequest_RackSatisfy\":$OffSwitchRequest_RackSatisfy," >> $outputjson
    echo -n "\"OffSwitchRequest_OffSwitchSatisfy\":$OffSwitchRequest_OffSwitchSatisfy," >> $outputjson
    echo -n "\"short_circuit_io_enabled\":\"null\"" >> $outputjson #don't know how to get this info, maybe parsing log
    echo -n "}," >> $outputjson
    # Node info
    if [ -e "$nodeinfotouse" ]; then
      cat $nodeinfotouse >> $outputjson
      echo -n "," >> $outputjson
    else
      echo "file $nodeinfotouse is not existed" >> $observerlogfile
      echo -n "\"nodeInfo\":{}" >> $outputjson
      echo -n "," >> $outputjson
    fi
    # Memory Usage
    echo -n "\"memory_usage\":" >> $outputjson
    echo -n "[" >> $outputjson
    echo -n ${Memoryload} >> $outputjson
    echo -n "]," >> $outputjson
    # CPU Usage
    echo -n "\"cpu_usage\":" >> $outputjson
    echo -n "[" >> $outputjson
    echo -n ${CPUload} >> $outputjson
    echo -n "]," >> $outputjson
    # Disk Usage
    echo -n "\"disk_usage\":" >> $outputjson
    echo -n "[" >> $outputjson
    echo -n ${Diskload} >> $outputjson
    echo -n "]," >> $outputjson
    # Network Usage
    echo -n "\"network_usage\":" >> $outputjson #TODO: modify nethogs to get info
    echo -n "[" >> $outputjson
    echo -n ${Networkload} >> $outputjson
    echo -n "]," >> $outputjson
    # Container State Developement
    echo -n "\"state_change\":\"null\"," >> $outputjson #TODO: parse log to know time container changes states
    # Logs
    echo -n "\"container_log\":" >> $outputjson
    echo -n "{" >> $outputjson
    echo -n "\"syslog\":" >> $outputjson
    echo -n "[" >> $outputjson

    echo -n "]," >> $outputjson
    echo -n "\"stderr\":\"null\"," >> $outputjson
    echo -n "\"stdout\":\"null\"" >> $outputjson
    echo -n "}" >> $outputjson
    # End
    echo -n "}" >> $outputjson
    echo "   [Done]"
    
    if [ -f ${LoadDir}/${container_id}.loadlog ]; then
      echo "Removing ${LoadDir}/${container_id}.loadlog"
      rm ${LoadDir}/${container_id}.loadlog
    fi
    if [ -f ${LoadDir}/${container_id}.nodeinfo ]; then
      echo "Removing ${LoadDir}/${container_id}.nodeinfo"
      rm ${LoadDir}/${container_id}.nodeinfo
    fi
    if [ -f ${LoadDir}/${container_id}.info ]; then
      echo "Removing ${LoadDir}/${container_id}.info"
      rm ${LoadDir}/${container_id}.info
    fi
    if [ -f ${LoadDir}/${container_id}.disklog ]; then
      echo "Removing ${LoadDir}/${container_id}.disklog"
      rm ${LoadDir}/${container_id}.disklog
    fi
    if [ -f ${LoadDir}/${container_id}.netlog ]; then
      echo "Removing ${LoadDir}/${container_id}.netlog"
      rm ${LoadDir}/${container_id}.netlog
    fi

  done
done

# Verifying the output
verifying=$( python ${TrackAppDir}/verify.py $appIdJsonDir )
echo -e "Verifying output file for $appId:\n$verifying" >> $observerlogfile

#Put the .jsonprofile to a store for easy access later
if [ "$elasticsearch_enabled" = "yes" ]; then
  ESServer=$elasticsearch_host
  ESPort=$elasticsearch_port
  if [ "$elasticsearch_daily_index" = "yes" ]; then
    today=$( date +"%d.%m.%y" ) #format DD.MM.YY
    ESIndex="${elasticsearch_index}-${today}"
  else
    ESIndex=$elasticsearch_index
  fi
  ESType=$elasticsearch_type
  ${TrackAppDir}/push2ES.sh $appIdJsonDir $ESServer $ESPort $ESIndex $ESType
  exitcode=$?
  if [ "$exitcode" = "0" ]; then #succeed
    if [ "$tar_enabled" = "no" ]; then #leave the deleting for tar_archive process
      rm -r $appIdJsonDir
    fi
  fi
fi

if [ "$tar_enabled" = "yes" ]; then
  echo "$appIdJsonDir" >> ${TrackAppDir}/tocompress.lst # add job to list of compressed file
  echo "Container log is stored at ${OutputDir}, waiting for compressing"
fi

#TODO: remove all temporary files
if [ -f ${TrackAppDir}/match_file_$appId ]; then
  rm ${TrackAppDir}/match_file_$appId
fi
