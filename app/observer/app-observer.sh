#!/bin/bash

##### Configuration
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export confdir="${DIR}/conf"
. "${DIR}/conf/configure.sh"

export observerdir="${DIR}" #TODO: be careful with this, export it one time in configure
today=$( date +"%m-%d-%y" )
loglocation=${observerdir}
logfile=${loglocation}/observer-${today}.log
export observerlogfile="$logfile"
SnapshotOutput="${observerdir}/SnapshotOutput"
JsonOutput="${observerdir}/output"
echo "Variables for the shell:" >> $logfile
export >> $logfile

function die {
  timestamp=$( date )
  echo "$timestamp ERROR: $1"
  exit 1 
}

function kerberos_authenticate {
  [ -f "$1" ] || die "Keytab file $1 does not exist!"
  kdestroy -A || die "Cannot destroy the cache" # remove all cache
  kinit -kt $1 $2 || die "Failed to authenticate to kerberos using keytab $1 as $2"
}

if [ "$kerberos" = "yes" ]; then
  if [ -e $kerberos_keytab ]; then
    NEGOTIATE_PAM="-i --negotiate -u:"
    #First time kinit
    kerberos_authenticate $kerberos_keytab $kerberos_principal >> $logfile
  else
    echo "Keytab not found, check if user is kinit_ed" >> $logfile
    klist >> $logfile
    NEGOTIATE_PAM="-i --negotiate -u:"
  fi
else
  NEGOTIATE_PAM=""
fi

if [ -f "$usage_log_dir" ]; then
  echo "File $usage_log_dir is created by trackers by mistake. Need to remove."
  now=$( date )
  echo "$now - Remove $usage_log_dir" >> $logfile
  rm $usage_log_dir 2>> $logfile
fi

if [ ! -d "$usage_log_dir" ]; then
  echo "Create directory $usage_log_dir to store usage logs"
  now=$( date )
  echo "$now - Create directory $usage_log_dir to store usage logs" >> $logfile
  mkdir -p $usage_log_dir
fi

if [ "$tar_enabled" = "yes" ]; then
  if [ "$duration" = "" ]; then
    echo "Duration is not set, set it to \"hourly\"" >> $logfile
    duration="hourly"
  fi
  ${observerdir}/tar-archive.sh $duration $SnapshotOutput & # get duration value from environment
  archive_process_pid=$! #for management maybe
fi

#detect activeRM
activeRM=$resourcemanager	#first use default
RMPort=$resourcemanagerport	#first use default

info=$( curl -L -s $NEGOTIATE_PAM "http://${activeRM}:${RMPort}/ws/v1/cluster/apps?state=RUNNING" | grep "apps" | tail -n 1 )

standbyRMsignal=$( echo "$info" | grep -c "This is standby RM. The redirect url is:" )
if [ "$standbyRMsignal" = "1" ]; then
  echo "$activeRM is not the active Resource Manager, switching to the new server now" >> $logfile
  activeRM=$( echo $info | awk '{print $NF}' | sed 's|http://||' | cut -d':' -f1 )
  if [ "$activeRM" = "" ]; then
    activeRM=$resourcemanager # use default RM 
    info=$( curl -L -s $NEGOTIATE_PAM "http://${activeRM}:${RMPort}/ws/v1/cluster/apps?state=RUNNING" | grep "apps" | tail -n 1 )
  else
    info=$( curl -s $NEGOTIATE_PAM "http://${activeRM}:${RMPort}/ws/v1/cluster/apps?state=RUNNING" | grep "apps" | tail -n 1 )
  fi
fi
if [ "$verbose" = "true" ]; then echo "Replies from RM $activeRM: $info"; fi
prev_app_list=$( echo $info | python ${observerdir}/getapplicationlist.py )

#main loop: keep track of application created on ResourceManager
prev_day=$( date +"%m-%d-%y" ) # to renew token for kerberos
prev_prev_clean_date="Feb 31" # impossible day for the begining
prev_clean_date="Feb 31"
while [ "1" = "1" ]; do
  
  info=$( curl -L -s $NEGOTIATE_PAM "http://${activeRM}:${RMPort}/ws/v1/cluster/apps?state=RUNNING" | grep "apps" | tail -n 1 )

  standbyRMsignal=$( echo "$info" | grep -c "This is standby RM. The redirect url is:" )
  if [ "$standbyRMsignal" = "1" ]; then
    echo "$activeRM is not the active Resource Manager, switching to the new server now" >> $logfile
    activeRM=$( echo $info | awk '{print $NF}' | sed 's|http://||' | cut -d':' -f1 )
    if [ "$activeRM" = "" ]; then
      activeRM=$resourcemanager # use default RM 
      info=$( curl -L -s $NEGOTIATE_PAM "http://${activeRM}:${RMPort}/ws/v1/cluster/apps?state=RUNNING" | grep "apps" | tail -n 1 )
    else
      info=$( curl -s $NEGOTIATE_PAM "http://${activeRM}:${RMPort}/ws/v1/cluster/apps?state=RUNNING" | grep "apps" | tail -n 1 )
    fi
    prev_app_list=""	#reset the list
    cur_app_list=""	#reset the list
  fi
  if [ "$verbose" = "true" ]; then echo "Replies from RM $activeRM: $info"; fi
  cur_app_list=$( echo $info | python ${observerdir}/getapplicationlist.py )

  now=$( date )
  if [ "$cur_app_list" != "" ]; then
    echo "$now - Running app: $cur_app_list"
  else
    echo "$now - No app is running"
  fi

  if [ "$cur_app_list" != "$prev_app_list"  ]; then
    for app in $prev_app_list; do
      found=0
      for new_app in $cur_app_list; do
        if [ "$app" = "$new_app" ]; then
          found=1
        fi
      done      
      if [ "$found" = '0' ]; then
        #app has disappeared
        echo "Generating container info for app $app"
        now=$( date )
        echo "$now - Generating container info for app $app" >> $logfile
        ( sleep 120 ; ${observerdir}/GenerateContainerInfo.sh $app $JsonOutput ) &
      fi
    done
  fi
  sleep 2
  prev_app_list=$cur_app_list
  
  today=$( date +"%m-%d-%y" )
  #handle format of the day to select file to delete. Have not test with system in French.
  #error format when day is less than 10. Need "Apr  6" (double space) not "Apr 06".
  dayonly=$( date +"%d" )
  if [ $dayonly -lt 10 ]; then
    monthonly=$( date +"%b" )
    let "dayonly=$dayonly+0" # convert 03 to 3
    current_clean_date="$monthonly  $dayonly" # "Apr  6"
  else
    current_clean_date=$( date +"%b %d" )  # "Apr 16"
  fi
  if [ "$prev_day" != "$today" ]; then # a new day begins
    
    #Renew kerberos token every day
    if [ "$kerberos" = "yes" ]; then
      echo "$(date): Renew the key for kerberos" >> $logfile
      klist >> $logfile
      kerberos_authenticate $kerberos_keytab $kerberos_principal >> $logfile
      echo "$(date): Current key for kerberos" >> $logfile
      klist >> $logfile
    fi
    
    #Rename log file for each day.
    logfile=${loglocation}/observer-${today}.log
    export observerlogfile="$logfile"
    prev_day=$today
    
    #Clean usage log directory by removing file that is created two days ago
    for usagelogfile in $( ls -lh ${usage_log_dir}/container*.* | grep "$prev_prev_clean_date" | awk '{print $NF}' ); do 
      echo "Removing ${usage_log_dir}/$usagelogfile"
      rm ${usage_log_dir}/$usagelogfile
    done
    prev_prev_clean_date="$prev_clean_date"
    prev_clean_date="$current_clean_date"

  fi
done

