#!/usr:bin/env python
from __future__ import print_function
import sys
import json
import re
import fileinput

def warning(*objs):
    print("WARNING: ", *objs, file=sys.stderr)

for line in fileinput.input():
    line = re.sub(' +',' ',line)
    my_param = line.split(" ")
    if len(my_param) == 26 and my_param[0] != 'TIME':
        TIME = my_param[0]
        CONTAINER_LOAD = my_param[24]
        PROCESS_STATUS = my_param[23]
        CPU_US = my_param[4][:-3]
        CPU_SY = my_param[5][:-3]
        CPU_NI = my_param[6][:-3]
        CPU_ID = my_param[7][:-3]
        CPU_WA = my_param[8][:-3]
        CPU_HI = my_param[9][:-3]
        CPU_SI = my_param[10][:-3]
        CPU_ST = my_param[11][:-3]
        SYSLOAD1M = my_param[1]
        SYSLOAD5M = my_param[2]
        SYSLOAD15M = my_param[3]
        print('{"capture_time":' + TIME + ',"container_load":' + CONTAINER_LOAD + ',"process_status":"' + PROCESS_STATUS + '","cpu_us":' + CPU_US + ',"cpu_sy":' + CPU_SY + ',"cpu_ni":' + CPU_NI + ',"cpu_id":' + CPU_ID + ',"cpu_wa":' + CPU_WA + ',"cpu_hi":' + CPU_HI + ',"cpu_si":' + CPU_SI + ',"cpu_st":' + CPU_ST + ',"sysload1m":' + SYSLOAD1M + ',"sysload5m":' + SYSLOAD5M + ',"sysload15m":' + SYSLOAD15M + '},', end='')
    else:
        warning("Cannot parse info")
