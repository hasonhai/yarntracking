#!/usr:bin/env python
from __future__ import print_function
import sys
import json
import re
import fileinput

def warning(*objs):
    print("WARNING: ", *objs, file=sys.stderr)

for line in fileinput.input():
    line = re.sub(' +',' ',line)
    my_param = line.split(" ")
    SWAPIN = "null"    
    if len(my_param) == 7 and my_param[0] != "EPOCHTIME":
        TIME = my_param[0]
        TOTALDISKREAD = my_param[1]
        TOTALDISKWRITE = my_param[2]
        PROCDISKREAD = my_param[3]
        PROCDISKWRITE = my_param[4]
        SWAPIN = my_param[5]
        IO = my_param[6].rstrip()
        ACTUALDISKREAD = "-1"
        ACTUALDISKWRITE = "-1"
    elif len(my_param) == 9 and my_param[0] != "EPOCHTIME":
        TIME = my_param[0]
        TOTALDISKREAD = my_param[1]
        TOTALDISKWRITE = my_param[2]
        PROCDISKREAD = my_param[5]
        PROCDISKWRITE = my_param[6]
        SWAPIN = my_param[7]
        IO = my_param[8].rstrip()
        ACTUALDISKREAD = my_param[3]
        ACTUALDISKWRITE = my_param[4]
    else:
        warning("Cannot parse info")
        continue 
    if SWAPIN == 'null':
        SWAPIN = "-1"
        IO = "-1"    
    print('{"capture_time":' + TIME + ',"total_disk_read":' + TOTALDISKREAD + ',"total_disk_write":' + TOTALDISKWRITE + ',"actual_disk_read":' + ACTUALDISKREAD + ',"actual_disk_write":' + ACTUALDISKWRITE + ',"container_disk_read":' + PROCDISKREAD + ',"container_disk_write":' + PROCDISKWRITE + ',"swap_in":' + SWAPIN + ',"io":' + IO + '},', end='')

