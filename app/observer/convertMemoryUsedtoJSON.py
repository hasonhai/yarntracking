#!/usr:bin/env python
from __future__ import print_function
import sys
import re
import json
import fileinput
from unitconverter import human2bytes

def warning(*objs):
    print("WARNING: ", *objs, file=sys.stderr)

for line in fileinput.input():
    line = " ".join(line.split())
    my_param = line.split(" ")
    if len(my_param) == 26 and my_param[0] != 'TIME':
        TIME = my_param[0]
        MEMTOTAL = str(human2bytes(my_param[12]))
        MEMUSED = str(human2bytes(my_param[13]))
        MEMFREE = str(human2bytes(my_param[14]))
        BUFFERS = str(human2bytes(my_param[15]))
        SWAPTOTAL = str(human2bytes(my_param[16]))
        SWAPUSED = str(human2bytes(my_param[17]))
        SWAPFREE = str(human2bytes(my_param[18]))
        SWAPCACHED = str(human2bytes(my_param[19]))
        VIRT = str(human2bytes(my_param[20]))
        RES = str(human2bytes(my_param[21]))
        SHR = str(human2bytes(my_param[22]))
        SWAP = str(human2bytes(my_param[25]))
        print('{"capture_time":' + TIME + ',"total_pmem":' + MEMTOTAL + ',"used_pmem":' + MEMUSED + ',"free_pmem":' + MEMFREE + ',"buffers_pmem":' + BUFFERS + ',"swap_total":' + SWAPTOTAL + ',"swap_used":' + SWAPUSED + ',"swap_free":' + SWAPFREE + ',"swap_cached":' + SWAPCACHED + ',"virt":' + VIRT + ',"res":' + RES + ',"shr":' + SHR + ',"swap":' + SWAP + '},', end='')
    else:
        warning("Cannot parse info")
#TODO: remove last comma ',' because no more record after that
