#!/bin/bash

#TODO: this file was not updated to support Kerberos

attemptId=$1
ResourceManager=$2
RMPort=$3
appId=$4

# first format of the link (on SPO cluster)
htmlpage=$( curl -s -XGET "http://$ResourceManager:$RMPort/cluster/appattempt/${attemptId}" )
# second format of the link (on PEI and Orange Portail)
# htmlpage=$( curl -s -XGET "http://$TimelineServer:$TSWebUIPort/cluster/apps/$appId/appattempts/${attemptId}" )
#  This is standby RM. The redirect url is: http://svr16.spo:8088/cluster/appattempt/appattempt_1456218758048_0021_000001
#  This is standby RM. The redirect url is: http://master002.current.rec.mapreduce.m1.p.fti.net:8088/cluster/apps/application_1465397593249_21449/appattempts/1
if [ "$( echo "$htmlpage" | grep -c "This is standby RM. The redirect url is:" )" = "1" ]; then
  url=$( echo "$htmlpage" | grep "This is standby RM. The redirect url is:" | awk '{print $NF}' )
  htmlpage=$( curl -s -XGET "$url" )
fi
# echo $htmlpage

AllocatedContainer=$( echo -e "$htmlpage" | grep "Total Allocated Containers:" | awk -F':' '{print $2}')

TableOpenLineIndex=$( echo -e "$htmlpage" | grep -n '<table id="containerLocality">' | awk -F':' '{print $1}' )
let LocalRequest_LocalSatisfy_LineNumber=$TableOpenLineIndex+20
let RackRequest_LocalSatisfy_LineNumber=$TableOpenLineIndex+34
let RackRequest_RackSatisfy_LineNumber=$TableOpenLineIndex+37
let OffSwitchRequest_LocalSatisfy_LineNumber=$TableOpenLineIndex+48
let OffSwitchRequest_RackSatisfy_LineNumber=$TableOpenLineIndex+51
let OffSwitchRequest_OffSwitchSatisfy_LineNumber=$TableOpenLineIndex+54

LocalRequest_LocalSatisfy=$( echo -e "$htmlpage" | head -n $LocalRequest_LocalSatisfy_LineNumber | tail -n 1 | awk '{print $1}' )
RackRequest_LocalSatisfy=$( echo -e "$htmlpage" | head -n $RackRequest_LocalSatisfy_LineNumber | tail -n 1 | awk '{print $1}' )
RackRequest_RackSatisfy=$( echo -e "$htmlpage" | head -n $RackRequest_RackSatisfy_LineNumber | tail -n 1 | awk '{print $1}' )
OffSwitchRequest_LocalSatisfy=$( echo -e "$htmlpage" | head -n $OffSwitchRequest_LocalSatisfy_LineNumber | tail -n 1 | awk '{print $1}' )
OffSwitchRequest_RackSatisfy=$( echo -e "$htmlpage" | head -n $OffSwitchRequest_RackSatisfy_LineNumber | tail -n 1 | awk '{print $1}' )
OffSwitchRequest_OffSwitchSatisfy=$( echo -e "$htmlpage" | head -n $OffSwitchRequest_OffSwitchSatisfy_LineNumber | tail -n 1 | awk '{print $1}' )
echo -n "$AllocatedContainer "
echo -n "$LocalRequest_LocalSatisfy "
echo -n "$RackRequest_LocalSatisfy "
echo -n "$RackRequest_RackSatisfy "
echo -n "$OffSwitchRequest_LocalSatisfy "
echo -n "$OffSwitchRequest_RackSatisfy "
echo "$OffSwitchRequest_OffSwitchSatisfy"
