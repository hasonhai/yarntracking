#!/usr/bin/python
import sys, getopt
import requests
import time
import json
import logging

logger = logging.getLogger()
logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
logger.setLevel(logging.DEBUG)
# Get the metric from the Resource Manager every second

RM_address = "svr02.spo"
RM_port = 8088
outfile = "clusterMetrics.csv"
duration = 1200

starttime = time.time()
URL = "http://" + RM_address + ":" + str(RM_port) + "/ws/v1/cluster/metrics"

timestamp = time.time()
while( (timestamp - starttime) < duration ):
    timestamp = time.time()

    try:
        response = requests.get( URL )
    except ConnectionError as e:
        # handle error when there is no response from server
        logging.warning("ConnectionError: {0}".format(e))
        time.sleep(1)
        continue
    except HTTPError as e:
        logging.warning("HTTPError: {0}".format(e))
        time.sleep(1)
        continue
    except Timeout as e:
        logging.warning("TimeoutError: {0}".format(e))
        time.sleep(1)
        continue
    except TooManyRedirects as e:
        logging.warning("TooManyRedirectError: {0}".format(e))
        time.sleep(1)
        continue

    try:
        response_json = json.loads(response.text)
    except ValueError as e:
        # handle error when the response is in json format
        logger.warning("Response from {0} does not have json valid format".format(url))
        time.sleep(1)
        continue
    
    if response_json["clusterMetrics"] is not None:
        appsSubmitted = response_json["clusterMetrics"]["appsSubmitted"]
        appsCompleted = response_json["clusterMetrics"]["appsCompleted"]
        appsPending = response_json["clusterMetrics"]["appsPending"]
        appsRunning = response_json["clusterMetrics"]["appsRunning"]
        appsFailed = response_json["clusterMetrics"]["appsFailed"]
        appsKilled = response_json["clusterMetrics"]["appsKilled"]
        reservedMB = response_json["clusterMetrics"]["reservedMB"]
        availableMB = response_json["clusterMetrics"]["availableMB"]
        allocatedMB = response_json["clusterMetrics"]["allocatedMB"]
        totalMB = response_json["clusterMetrics"]["totalMB"]
        reservedVirtualCores = response_json["clusterMetrics"]["reservedVirtualCores"]
        availableVirtualCores = response_json["clusterMetrics"]["availableVirtualCores"]
        allocatedVirtualCores = response_json["clusterMetrics"]["allocatedVirtualCores"]
        totalVirtualCores = response_json["clusterMetrics"]["totalVirtualCores"]
        containersAllocated = response_json["clusterMetrics"]["containersAllocated"]
        containersReserved = response_json["clusterMetrics"]["containersReserved"]
        containersPending = response_json["clusterMetrics"]["containersPending"]
        totalNodes = response_json["clusterMetrics"]["totalNodes"]
        activeNodes = response_json["clusterMetrics"]["activeNodes"]
        lostNodes = response_json["clusterMetrics"]["lostNodes"]
        unhealthyNodes = response_json["clusterMetrics"]["unhealthyNodes"]
        decommissionedNodes = response_json["clusterMetrics"]["decommissionedNodes"]
        rebootedNodes = response_json["clusterMetrics"]["rebootedNodes"]
    
        #export to csv file
        metrics = "{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14};{15};{16};{17};{18};{19};{20};{21};{22};{23}\n".format(
                         timestamp,appsSubmitted,appsCompleted,appsPending,appsRunning,appsFailed,appsKilled,reservedMB,availableMB,
                         allocatedMB,totalMB,reservedVirtualCores,availableVirtualCores,allocatedVirtualCores,totalVirtualCores,
                         containersAllocated,containersReserved,containersPending,totalNodes,activeNodes,lostNodes,
                         unhealthyNodes,decommissionedNodes,rebootedNodes)
        with open(outfile, "a") as text_file:
            text_file.write(metrics)
            logger.info("print cluster metrics to {0}: {1}".format(outfile, metrics))
        # TODO:we have not handdled the exception for open file here
    else:
       logger.warning("Cannot read the metrics from cluster")

    time.sleep(1)
