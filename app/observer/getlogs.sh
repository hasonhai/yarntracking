#!/bin/bash
appId=$1     #required
appOwner=$2  #required
HDFSadmin=$3 #required, user should have passwordless sudo right to immitate HDFS user
dir4Logs=$4
if [ ! $4 ]; then
  dir4Logs=$1
fi

# Parsing containers' logs
# Using "yarn logs -applicationId $applicationid" to get the log on HDFS
echo Collect application logs from HDFS...
if [ ! -d "$dir4Logs" ]; then
  echo -n "Directory $dir4Logs is not exist, creating..."
  mkdir -p $dir4Logs
  echo "   [done]"
fi
appLog="${dir4Logs}/${appId}.log" # Log from all container will be stored here

#TODO: have not implemented for kerberos cluster

if [ "$sudo_enabled" = "yes" ]; then
  if [ "$( sudo -v | grep -c "Sorry," )" != "1" ]; then #get permission to see logs
    sudo runuser -l $HDFSadmin -c "yarn logs -appOwner $appOwner -applicationId $appId" > $appLog
  else
    echo $( whoami ) does not have sudo right!
    exit 1
  fi
else
  if [ "$hdfs_user" != "$( whoami )" ]; then
    echo "Please specify the correct hdfs admin user in yarntracking.conf"
    exit 1
  else
    #TODO: check if current user have hdfs admin right
    echo "We set current user as the admin user of HDFS"
  fi
fi
# List of containers and servers executing them
grep "Container: container_" $appLog | cut -d' ' -f2,4 --output-delimiter='_' | cut -d'_' -f1,2,3,4,5,6 > ${dir4Logs}/containername
grep "Container: container_" $appLog | cut -d' ' -f2,4,6 --output-delimiter='_' | cut -d'_' -f7 > ${dir4Logs}/container_server
# Starting line of each container
grep -n "Container: container_" $appLog | cut -d':' -f1 > ${dir4Logs}/lineindexstart   # Find where the container's log start
tail -n +2 ${dir4Logs}/lineindexstart | gawk '{print $1-1}' > ${dir4Logs}/lineindexend # Find where the container's log end
wc -l $appLog | cut -d' ' -f1 >> ${dir4Logs}/lineindexend
paste -d' ' ${dir4Logs}/containername ${dir4Logs}/lineindexstart ${dir4Logs}/lineindexend ${dir4Logs}/container_server > ${dir4Logs}/containers # Merge to onefile
rm -f ${dir4Logs}/containername ${dir4Logs}/lineindexstart ${dir4Logs}/lineindexend ${dir4Logs}/container_server
echo -n "Separate each container's logs to each syslog file..."
while read containerinfo; do
    containername=`echo "$containerinfo" | gawk '{print $1}'`
    servername=`echo "$containerinfo" | gawk '{print $4}'`
    startline=`echo "$containerinfo" | gawk '{print $2}'`
    endline=`echo "$containerinfo" | gawk '{print $3}'`
    sed -n "${startline},${endline}p" $appLog > ${dir4Logs}/${containername}_${servername}.log
    num=`grep -n -m 3 "Log Contents:" ${dir4Logs}/${containername}_${servername}.log | cut -d':' -f1 | tail -n 1 | gawk '{ print $1+1 }'`
    tail -n +$num ${dir4Logs}/${containername}_${servername}.log > ${dir4Logs}/${containername}_${servername}.syslog
    rm -f ${dir4Logs}/${containername}_${servername}.log
done < ${dir4Logs}/containers
echo "   [done]"
