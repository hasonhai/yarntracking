import json
import requests
import sys
from sys import argv

script, hsserver, hsport, jobid, taskid = argv

url = "http://%s:%s/ws/v1/history/mapreduce/jobs/%s/tasks/%s/counters" % (hsserver, hsport, jobid, taskid)

data = requests.get(url).text
taskcounter_json = json.loads(data)

taskcounter_dict = {}
if taskcounter_json["jobTaskCounters"]["id"] != None:
  for countergroup in taskcounter_json["jobTaskCounters"]["taskCounterGroup"]:
    for counter in countergroup["counter"]:
        taskcounter_dict[counter["name"]] = counter["value"]

taskcounter_json = json.dumps(taskcounter_dict, ensure_ascii=False, sort_keys=True, separators=(',', ':'))
sys.stdout.write(taskcounter_json)

