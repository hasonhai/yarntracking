import json
import requests
from sys import argv

script, server, port, jobid = argv

url = "http://%s:%s/ws/v1/history/mapreduce/jobs/%s/tasks" % (server, port, jobid)

data = requests.get(url).text
tasklist_json = json.loads(data)

if tasklist_json["tasks"] != None:
  for task in tasklist_json["tasks"]["task"]:
    tmp_url = url + "/" + task["id"] + "/attempts/attempt_" + task["id"][5:] + "_0"
    container_info = requests.get(tmp_url).text #test with first attempt only
    container_json = json.loads(container_info)
    print task["id"] + " " + container_json["taskAttempt"]["assignedContainerId"]
    

