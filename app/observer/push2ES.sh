#!/bin/bash
if [ $# -ne 5 ]; then
  echo "Number of variables: $#, syntax is not correct"
  echo " 1st arg: fullpath to json folder"
  echo " 2nd arg: elasticsearch server"
  echo " 3rd arg: elasticsearch port"
  echo " 4th arg: elasticsearch index for this yarntracking app"
  echo " 5th arg: elasticsearch type for these records"
  exit 1
else
  inputdir=$1	#fullpath to json folder
  server=$2	#elasticsearch server
  port=$3	#elasticsearch port
  index=$4	#elasticsearch index for this yarntracking app
  type=$5	#elasticsearch type for these records
  log=$observerlogfile #from environement
  if [ "$log" = "" ]; then log=ESpush.log; fi

  for file in $( ls $inputdir ); do
    if [ "${file}" = "requests" ]; then continue; fi
    echo "Pushsing file $file to elasticsearch"
    id=$( echo $file | cut -d'.' -f1 )
    echo -e "{ \"index\" : { \"_index\" : \"$index\", \"_type\" : \"$type\", \"_id\" : \"$id\" } }" > ${inputdir}/requests
    cat $inputdir/$file >> ${inputdir}/requests
    echo "" >> ${inputdir}/requests
    respond=$( curl -s -XPOST ${server}:${port}/_bulk --data-binary @${inputdir}/requests )
    echo "$respond" >> $log
  done
  rm ${inputdir}/requests
  exit 0
fi
