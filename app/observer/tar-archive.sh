#!/bin/bash
duration=$1 #hourly or daily
if [ ! -z "$2" ]; then
  SnapshotOutput="$2"
else
  SnapshotOutput="$observerdir/SnapshotOutput"
fi
if [ ! -e $SnapshotOutput ]; then
  mkdir -p $SnapshotOutput
fi

#make sure kill all other tar-archive process
mypid=$$
listpid=$( ps aux | grep -e "[/]tar-archive.sh" | awk '{print $2}' )
if [ $( echo "$listpid" | wc -w ) -gt 2 ]; then
  if [ "$verbose" = "true" ]; then detaillist=$( ps aux | grep -e "[/]tar-archive.sh" ); echo -e "$detaillist" >> $observerlogfile; fi
  echo -e "My PID is ${mypid}. There are many tar-archive processes running on this node:\n$listpid" >> $observerlogfile
  for pid in $listpid; do
    if [ "$pid" != "$mypid" ]; then
      echo "Killing tar-archive process with PID $pid" >> $observerlogfile
      kill $pid && echo "Done killing $pid" >> $observerlogfile
    fi
  done
  if [ "$verbose" = "true" ]; then detaillist=$( ps aux | grep -e "[/]tar-archive.sh" ); echo -e "$detaillist" >> $observerlogfile; fi
fi

prev_time=$( date +"%Y%m%d-%H" )
while [ "1" = "1" ]; do
  sleep 300

  current_time=$( date +"%Y%m%d-%H" )
  if [ "$duration" = "hourly" ]; then
    filetime="$current_time"
  elif [ "$duration" = "daily" ]; then
    filetime=$( echo "$current_time" | cut -d"-" -f1 )
  else
    duration="hourly" #set to default
  fi
  snapshotname="ClusterSnapshot-$filetime"

  #format the value to make comparison
  if [ $duration = "hourly" ]; then # reformat to get the day part only
    prev_time_cp="$prev_time"
    current_time_cp="$current_time"
  else #reformat to get the day part only
    prev_time_cp=$( echo "$prev_time" | cut -d'-' -f1 )
    current_time_cp=$( echo "$current_time" | cut -d'-' -f1 )
  fi

  if [ "$verbose" = "true" ]; then
    echo "duration: $duration"
    echo "prev_time: $prev_time"
    echo "prev_time_cp: $prev_time_cp"
    echo "current_time: $current_time"
    echo "current_time_cp: $current_time_cp"
  fi

  if [ "$prev_time_cp" != "$current_time_cp" ]; then
    if [ -e ${observerdir}/tocompress.lst ]; then
      echo "Making archive file for the logs with snapshot ${snapshotname}.tar.gz" >> $observerlogfile
      mv ${observerdir}/tocompress.lst ${observerdir}/tocompress.lsttmp
      if [ -e "${SnapshotOutput}/${snapshotname}.tar.gz" ]; then #if file exist
        epoch=$( date +%s )
        snapshotname="${snapshotname}-${epoch}" #append epoch to snapshotname to prevent overwrite
      fi
      tar -pczf ${SnapshotOutput}/${snapshotname}.tar.gz -T ${observerdir}/tocompress.lsttmp 2>> $observerlogfile
      exitcodeTar=$?
      if [ $exitcodeTar -eq 0 ]; then
        for appjsondir in $( cat ${observerdir}/tocompress.lsttmp ); do # remove all directories that are processed
          echo "Remove directory $appjsondir"
          rm -r $appjsondir
        done
        echo "Successfully created the snapshot ${snapshotname}.tar.gz" >> $observerlogfile
      else
        echo -e "ERROR: Failed to create the snapshot ${snapshotname}.tar.gz which includes the following job logs:\n$(cat ${observerdir}/tocompress.lsttmp )" >> $observerlogfile
      fi
      rm ${observerdir}/tocompress.lsttmp
    fi
  fi
  prev_time="$current_time"
done

