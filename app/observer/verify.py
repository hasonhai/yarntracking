import json
import os, sys
from sys import argv

script, jsonpath = argv

invalid_json_files = []
read_json_files = []

def parse(jsonpath):
    for files in os.listdir(jsonpath):
        filepath = jsonpath + '/' + files
        with open(filepath) as json_file:
            try:
                json.load(json_file)
                read_json_files.append(files)
            except ValueError, e:
                print ("JSON object issue: %s") % e
                invalid_json_files.append(files)
    appname = os.path.basename(jsonpath)
    print ("%s has %d containers with invalid format:" % (appname, len(invalid_json_files)))
    print invalid_json_files
    print ("%s has %d containers with valid format:" % (appname, len(read_json_files)))
    print read_json_files

parse(jsonpath)
