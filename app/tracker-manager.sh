#!/bin/bash

###### Tracker Manager ########

###### Configure
curdir=`dirname "$0"`
DIR=`cd $curdir; pwd`
export confdir="${DIR}/conf"
. "${DIR}/conf/configure.sh"

########################################
trackerscriptdir=$DIR/tracker #change this when reorginize the code
observerscriptdir=$DIR/observer
TRACKER_SCREENNAME=tracker
OBSERVER_SCREENNAME=observer
managerlog=$DIR/manager.log
function usage {
  echo wrong syntax
  echo "$(basename $0) [start|stop*|force-stop|status|update-script] [trackers|observer]"
  echo "$(basename $0) [start|stop*|force-stop|status|update-script] tracker nodename"
  echo "$(basename $0) kinit observer"
  echo "    start: start scripts on all trackers/observer/tracker"
  echo "    *stop: stop scripts on all trackers/observer/tracker by sending Ctrl-C to script running in screen session"
  echo "    force-stop: stop scripts on all trackers/observer by kill screen PID"
  echo "    status: get status of script running on trackers/observer"
  echo "    update-script: update scripts on all trackers/observer by copy scripts inside tracker directory"
  echo "    clean: clean all the file created by this script. BE CAREFUL: rm -r ~/scriptdirectory"
  echo "    kinit observer: allow app to run in kerberized cluster without keytab (until token expired)"
  exit 1
}

function start_node {
  host=$1
  mission=$2
  echo "Starting node $host functioning as $mission"
}

function stop_node {
  host=$1
  mission=$2
  echo "Stopping node $host functioning as $mission"
}

function get_status_node {
  host=$1
  mission=$2
  echo "Node $host is functioning as $mission and is $status"
}

function clean_node {
  host=$1
  workdir=$2
  echo "Cleaning node $host by removing $workdir"
}

function update_node {
  host=$1
  echo "Updating node $host with new script"
}

command=$1
obj=$2

if [ "$obj" = "tracker" ]; then
  node=$3
  if [ "$node" = "" ]; then
    echo "You need to specify the node, use \"trackers\" if you meant all the trackers"
    usage
  fi
fi 

if [ "$command" = "stop" ]; then
  command="force-stop"
fi

if [[ ($command = "start") && ($obj = "trackers") ]]; then
  for host in $( cat $trackerlist ); do
    echo "start script at tracker $host"
    ssh -i $access_key $user@$host "screen -d -m -S $TRACKER_SCREENNAME 'tracker/detectNewContainer.sh'"
  done
elif [[ ($command = "stop") && ($obj = "trackers") ]]; then #this one looks like does not work. screen has error with stuff command
  for host in $( cat $trackerlist ); do
    echo "stop script at tracker $host"
    screenPIDlst=$( ssh -i $access_key $user@$host "screen -ls | grep $TRACKER_SCREENNAME | cut -d. -f1" )
    for screenPID in $screenPIDlst; do
      echo send SIGINT to windows: ${screenPID}.${TRACKER_SCREENNAME}
      ssh -i $access_key $user@$host "screen -S ${screenPID}.${TRACKER_SCREENNAME} -X stuff $'\003'"
    done
  done
elif [[ ($command = "force-stop") && ($obj = "trackers") ]]; then
  for host in $( cat $trackerlist ); do
    echo "stop script at tracker $host"
    screenPIDlst=$( ssh -i $access_key $user@$host "screen -ls | grep $TRACKER_SCREENNAME | cut -d. -f1" )
    for screenPID in $screenPIDlst; do
      echo kill screen PID: ${screenPID}.${TRACKER_SCREENNAME}
      ssh -i $access_key $user@$host "kill ${screenPID}"
    done
  done
elif [[ ($command = "status") && ($obj = "trackers") ]]; then
  for host in $( cat $trackerlist ); do
    scriptstatus=$( ssh -i $access_key $user@$host 'ps aux | grep -c "[/]bin/bash tracker/detectNewContainer.sh"' )
    sessionstatus=$( ssh -i $access_key $user@$host 'screen -ls | grep -c "[t]racker"' )
    if [[ ($scriptstatus -eq 1) && ($sessionstatus -eq 1) ]]; then
      echo "tracking script on $host is up"
    else
      echo "tracking script on $host is down with error code ${scriptstatus}${sessionstatus} (or not started)"
    fi
  done
elif [[ ($command = "update-script") && ($obj = "trackers") ]]; then
  for host in $( cat $trackerlist ); do
    cp -r $confdir $trackerscriptdir/
    rm -f $trackerscriptdir/conf/*.keytab 2> /dev/null #remove keytab file if there is
    scp -i $access_key -r $trackerscriptdir $user@$host: #update script to the newest one
    rm -r $trackerscriptdir/conf #remove after copy
  done
elif [[ ($command = "clean") && ($obj = "trackers") ]]; then
  for host in $( cat $trackerlist ); do
    echo "Removing \"tracker\" directory on $host"
    ssh -i $access_key $user@$host "rm -r tracker"  #remove tracker script
  done
#Control a specific node
elif [[ ($command = "start") && ($obj = "tracker") ]]; then
  host=$node
  echo "start script at tracker $host"
  ssh -i $access_key $user@$host "screen -d -m -S $TRACKER_SCREENNAME 'tracker/detectNewContainer.sh'"
elif [[ ($command = "stop") && ($obj = "tracker") ]]; then #this one looks like does not work. screen has error with stuff command
  host=$node
  echo "stop script at tracker $host"
  screenPIDlst=$( ssh -i $access_key $user@$host "screen -ls | grep $TRACKER_SCREENNAME | cut -d. -f1" )
  for screenPID in $screenPIDlst; do
    echo send SIGINT to windows: ${screenPID}.${TRACKER_SCREENNAME}
    ssh -i $access_key $user@$host "screen -S ${screenPID}.${TRACKER_SCREENNAME} -X stuff $'\003'"
  done
elif [[ ($command = "force-stop") && ($obj = "tracker") ]]; then
  host=$node
  echo "stop script at tracker $host"
  screenPIDlst=$( ssh -i $access_key $user@$host "screen -ls | grep $TRACKER_SCREENNAME | cut -d. -f1" )
  for screenPID in $screenPIDlst; do
    echo kill screen PID: ${screenPID}.${TRACKER_SCREENNAME}
    ssh -i $access_key $user@$host "kill ${screenPID}"
  done
elif [[ ($command = "status") && ($obj = "tracker") ]]; then
  host=$node
  scriptstatus=$( ssh -i $access_key $user@$host 'ps aux | grep -c "[/]bin/bash tracker/detectNewContainer.sh"' )
  sessionstatus=$( ssh -i $access_key $user@$host 'screen -ls | grep -c "[t]racker"' )
  if [[ ($scriptstatus -eq 1) && ($sessionstatus -eq 1) ]]; then
    echo "tracking script on $host is up"
  else
    echo "tracking script on $host is down with error code ${scriptstatus}${sessionstatus} (or not started)"
  fi
elif [[ ($command = "update-script") && ($obj = "tracker") ]]; then
  host=$node
  cp -r $confdir $trackerscriptdir/
  rm -f $trackerscriptdir/conf/*.keytab 2> /dev/null #remove keytab file if there is
  scp -i $access_key -r $trackerscriptdir $user@$host: #update script to the newest one
  rm -r $trackerscriptdir/conf #remove after copy
elif [[ ($command = "clean") && ($obj = "tracker") ]]; then
  host=$node
  echo "Removing \"tracker\" directory on $host"
  ssh -i $access_key $user@$host "rm -r tracker"  #remove tracker script
#Control the master service
elif [[ ($command = "start") && ($obj = "observer") ]]; then
  host=$observer
  echo "start script at observer $host"
  ssh -i $access_key $user@$host "screen -d -m -S $OBSERVER_SCREENNAME 'observer/app-observer.sh'"
elif [[ ($command = "stop") && ($obj = "observer") ]]; then #this one looks like does not work. screen has error with stuff command
  host=$observer
  echo "stop script at observer $host"
  screenPIDlst=$( ssh -i $access_key $user@$host "screen -ls | grep $OBSERVER_SCREENNAME | cut -d. -f1" )
  for screenPID in $screenPIDlst; do
    echo send SIGINT to windows: ${screenPID}.${OBSERVER_SCREENNAME}
    ssh -i $access_key $user@$host "screen -S ${screenPID}.${OBSERVER_SCREENNAME} -X stuff $'\003'"
  done
elif [[ ($command = "force-stop") && ($obj = "observer") ]]; then
  host=$observer
  echo "stop script at observer $host"
  screenPIDlst=$( ssh -i $access_key $user@$host "screen -ls | grep $OBSERVER_SCREENNAME | cut -d. -f1" )
  for screenPID in $screenPIDlst; do
    echo kill screen PID: ${screenPID}.${OBSERVER_SCREENNAME}
    ssh -i $access_key $user@$host "kill ${screenPID}"
  done
elif [[ ($command = "status") && ($obj = "observer") ]]; then
  host=$observer
  scriptstatus=$( ssh -i $access_key $user@$host 'ps aux | grep -c "[/]bin/bash observer/app-observer.sh"' )
  sessionstatus=$( ssh -i $access_key $user@$host 'screen -ls | grep -c "[o]bserver"' )
  if [[ ("$scriptstatus" -ge 1) && ("$sessionstatus" -eq 1) ]]; then
    echo "Observer script on $host is up"
  else
    echo "Observer script on $host is down with error code ${scriptstatus}${sessionstatus} (or not started)"
  fi 
elif [[ ("$command" = "update-script") && ("$obj" = "observer") ]]; then
  host=$observer
  echo "Copy new observer scripts to $host"
  cp -r $confdir $observerscriptdir/
  scp -i $access_key -r $observerscriptdir $user@$host: #update script to the newest one
  rm -r $observerscriptdir/conf
elif [[ ("$command" = "clean") && ("$obj" = "observer") ]]; then
  host=$observer
  echo "Removing \"observer\" directory on $observer"
  ssh -i $access_key $user@$host "rm -r observer"  #remove script
elif [[ ("$command" = "kinit") && ("$obj" = "observer") ]]; then
  echo "Kinit $user"
  ssh -i $access_key $user@$obj "kinit"
else
  usage
fi
