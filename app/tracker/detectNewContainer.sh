#!/bin/bash

###### Configure
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export confdir="${DIR}/conf"
. "${DIR}/conf/configure.sh"

delay=1
today=$( date +"%m-%d-%y" )
applog=$DIR/tracker-$(hostname)-${today}.log
if [ $1 ]; then
  loadlogpath="$1"
else
  loadlogpath="$DIR" #temp directory for loadlogfile
fi
echo select $loadlogpath to store loadlog file
echo select $loadlogpath to store loadlog file >> $applog

NodeManagerPID=$( ps ax | grep "[o]rg.apache.hadoop.yarn.server.nodemanager.NodeManager" | awk '{print $1}' )
echo "NodeManager PID is: $NodeManagerPID"
if [ "${NodeManagerPID}" = "" ]; then
  echo "There is no NodeManager running on this node: PID = $NodeManagerPID" >> $applog
  echo "There is no NodeManager running on this node: PID = $NodeManagerPID"
  exit 1
fi

$DIR/getnodeinfo.sh defaults ${loadlogpath}
nodeinfofile="${loadlogpath}/defaults.nodeinfo"
locateddir="${usage_log_dir}"
host=$( hostname -f )
newname=${host}.nodeinfo
if [ ! -e $nodeinfofile ]; then
  echo "Cannot create default file for nodeinfo" >> $applog
else
  ssh -i $access_key $user@$observer "if [ ! -e $locateddir  ]; then mkdir -p $locateddir; fi"
  scp -i $access_key ${nodeinfofile} $user@$observer:$locateddir/$newname 2>> $applog
fi

prev_day=$( date +"%m-%d-%y" )
prev_clean_date="Feb 31"
prev_prev_clean_date="Feb 31"

oldstate='{"containers":null}'
old_container_list=''
while [ "1" = "1" ]; do # now always run
  newstate=$( curl -s -XGET http://localhost:8042/ws/v1/node/containers )
  currenttime=$( date )
  if [ "$oldstate" = "$newstate" ]; then
    echo "$currenttime: Nothing is new during this period"
  else
    echo "$currenttime: There may be new containers on this node"
    echo "NodeManager response: $newstate"
    # get list of container
    new_container_list=$( echo $newstate | python ${DIR}/getcontainerlist.py )
    new_containers=''
    if [[ ("$new_container_list" != '') && ("$old_container_list" = '') ]]; then
      # run all the container in new_container_list
      new_containers="$new_container_list"
      echo "$currenttime: adding container(s) to list of new containers:"
      echo "$new_containers"
      echo "$currenttime: adding container(s) to list of new containers: $new_containers" >> $applog
    elif [[ ("$new_container_list" = '') && ("$old_container_list" != '') ]]; then
      new_containers=''
    elif [[ ("$new_container_list" = '') && ("$old_container_list" = '') ]]; then
      new_containers=''
    else
      # compare with the old list of container, look for new container
      for containerA in $new_container_list; do
        match=false
        for containerB in $old_container_list; do
          if [ "$containerB" = "$containerA" ]; then
            match=true
            break
          fi
        done
        if [ "$match" = false ]; then
          new_containers="$new_containers $containerA"
          echo "$currenttime: adding container to list of new containers: $new_containers"
          echo "$currenttime: adding container to list of new containers: $new_containers" >> $applog
        fi
      done
    fi
    # for each new container, call tracking process
    if [ "$new_containers" != '' ]; then
      for container in $new_containers; do
        $DIR/trackingContainerByID.sh $container ${loadlogpath} $applog &
        trackid=$!
        echo "$currenttime: Start tracking container ${container} with process $trackid"
        echo "$currenttime: Start tracking container ${container} with process $trackid" >> $applog
        $DIR/trackingCompletionAndSend.sh $trackid $container ${loadlogpath} $applog &
      done
    fi
    old_container_list=$new_container_list
  fi
  oldstate="$newstate"
  sleep $delay

  today=$( date +"%m-%d-%y" )
  #handle format of the day to select file to delete. Have not test with system in French.
  #error format when day is less than 10. Need "Apr  6" (double space) not "Apr 06".
  dayonly=$( date +"%d" )
  if [ $dayonly -lt 10 ]; then
    monthonly=$( date +"%b" )
    let "dayonly=$dayonly+0" # convert 03 to 3
    current_clean_date="$monthonly  $dayonly" # "Apr  6"
  else
    current_clean_date=$( date +"%b %d" )  # "Apr 16"
  fi
  if [ "$prev_day" != "$today" ]; then # a new day begins

    #Rename log file for each day.
    applog="$DIR/tracker-$(hostname)-${today}.log"
    export applog="$applog"
    prev_day="$today"

    #Clean directory by removing file that is created two days ago
    echo "Clean the working directory for today, yesterday is $prev_clean_date, date before is $prev_prev_clean_date" >> $applog
    for usagelogfile in $( ls -lh ${DIR}/container*.* | grep "$prev_prev_clean_date" | awk '{print $NF}' ); do
      echo "Removing ${DIR}/$usagelogfile" >> $applog
      rm ${DIR}/$usagelogfile
    done
    prev_prev_clean_date="$prev_clean_date"
    prev_clean_date="$current_clean_date"

  fi

done
