#!/bin/bash
container_id=$1
storeddir=$2
jsonoutput=${storeddir}/${container_id}.nodeinfo
if [ $3 ]; then 
  NMhost=$3 
else 
  NMhost=0.0.0.0
fi
if [ $4 ]; then
  NMport=$4;
else 
  NMport=8042
fi

#lscpu # list cpu info

totalcore=$( nproc ) # number of core
totalmem=$( cat /proc/meminfo | grep "MemTotal" | awk '{print $2}' )
nodeinfo=$( curl -s -XGET "http://${NMhost}:${NMport}/ws/v1/node/info" )

if [ "$nodeinfo" != "" ]; then
  totalVmemAllocatedContainersMB=$( echo "$nodeinfo" | cut -d',' -f2 | cut -d':' -f2 )
  totalPmemAllocatedContainersMB=$( echo "$nodeinfo" | cut -d',' -f3 | cut -d':' -f2 )
  totalVCoresAllocatedContainers=$( echo "$nodeinfo" | cut -d',' -f4 | cut -d':' -f2 )
  vmemCheckEnabled=$( echo "$nodeinfo" | cut -d',' -f5 | cut -d':' -f2 )
  pmemCheckEnabled=$( echo "$nodeinfo" | cut -d',' -f6 | cut -d':' -f2 )
  nodeManagerVersion=$( echo "$nodeinfo" | cut -d',' -f9 | cut -d':' -f2 )
  hadoopVersion=$( echo "$nodeinfo" | cut -d',' -f12 | cut -d':' -f2 )
  id=$( echo "$nodeinfo" | cut -d',' -f15 | cut -d':' -f2,3 --output-delimiter=':' )
  nodeHostName=$( echo "$nodeinfo" | cut -d',' -f16 | cut -d':' -f2 | sed 's/}//g' )
else
  echo cannot get node info
  exit 1
fi

echo -n "\"nodeInfo\":" > $jsonoutput
echo -n "{" >> $jsonoutput
echo -n "\"totalNodePhyCore\":$totalcore," >> $jsonoutput
echo -n "\"totalNodePhyMem\":$totalmem," >> $jsonoutput
echo -n "\"totalVmemAllocatedContainersMB\":$totalVmemAllocatedContainersMB," >> $jsonoutput
echo -n "\"totalPmemAllocatedContainersMB\":$totalPmemAllocatedContainersMB," >> $jsonoutput
echo -n "\"totalVCoresAllocatedContainers\":$totalVCoresAllocatedContainers," >> $jsonoutput
echo -n "\"vmemCheckEnabled\":$vmemCheckEnabled," >> $jsonoutput
echo -n "\"pmemCheckEnabled\":$pmemCheckEnabled," >> $jsonoutput
echo -n "\"nodeManagerVersion\":$nodeManagerVersion," >> $jsonoutput
echo -n "\"hadoopVersion\":$hadoopVersion," >> $jsonoutput
echo -n "\"id\":$id," >> $jsonoutput
echo -n "\"nodeHostName\":$nodeHostName" >> $jsonoutput
echo -n "}" >> $jsonoutput
