#!/bin/bash
# This script is used to capture the disk bandwidth during the time the process is running
#    TotalDISKREAD and TotalDISKWRITE: Bandwidth between process and kernel subsystem
#    ActualDISKREAD and ActualDISKWRITE: Bandwidth between kernel subsystem and underlying hardware
#    ProcDISKREAD and ProcDISKWRITE: Bandwidth process actually uses
#    SwapIn:
#    IO:
#    Notes, in Centos there is no different between TDR and ADR, or TDW and ADW

PID=$1
container_id=$2
storeddir=$3
disklogfile=${storeddir}/${container_id}.disklog
tmpfile=${storeddir}/diskio_${PID}.log
osversion="centos" #or "ubuntu"

testiotop="$( which iotop | grep -c "no iotop in" )"
if [ "$testiotop" = "1" ]; then
  echo "IOtop not found on this worker"
  exit 1
fi

testsudo="nosudo"
if [ "$sudo_enabled" = "yes" ]; then
  testsudo=$( sudo -v )
else
  echo "Use of sudo is not enabled"
  exit 1
fi

if [ "$testsudo" != "" ]; then
  echo "User $( whoami ) does not have right to use iotop"
  exit 1
fi

#Date format: 2016-06-24
CURRENT_DATE=$( date +"%Y-%m-%d" )
sudo iotop -b -p $PID -d 1 -k -t -qq > $tmpfile &
iotop_PID=$!
while [ -e /proc/$PID ]; do # wait for task to finish
  sleep 1
done
sudo kill ${iotop_PID} && echo "Killing ${iotop_PID} successfully"
 
sed -i -e "s/[[:space:]]\+/ /g" $tmpfile
sed -i -e ':a' -e 'N' -e '$!ba' -e 's/\n/ /g' $tmpfile
sed -i 's/Total DISK READ/\nTotal DISK READ/g' $tmpfile
sed -i '/^ *$/d' $tmpfile

if [ "$osversion" = "ubuntu" ]; then
  echo "EPOCHTIME TotalDISKREAD TotalDISKWRITE ActualDISKREAD ActualDISKWRITE ProcDISKREAD ProcDISKWRITE SwapIn IO" > $disklogfile
  #TODO: handle for Ubuntu
else #for centos
  echo "EPOCHTIME TotalDISKREAD TotalDISKWRITE ProcDISKREAD ProcDISKWRITE SwapIn IO" > $disklogfile
  while read line
  do
    TotalDISKREAD=$( echo "$line" | awk '{print $4}' )
    TotalDISKWRITE=$( echo "$line" | awk '{print $10}' )
    TIME=$( echo "$line" | awk '{print $12}' )
    ProcDISKREAD=$( echo "$line" | awk '{print $16}' )
    ProcDISKWRITE=$( echo "$line" | awk '{print $18}' )

    SwapInInfo=$( echo "$line" | grep "?unavailable?" )
    if [ "$SwapInInfo" != "" ]; then
      #echo Swap info is unavailable
      line=$( echo $line | sed 's/?unavailable?.*/null null null null/g' )
    fi
    SwapIn=$( echo "$line" | awk '{print $20}' )
    IO=$( echo "$line" | awk '{print $22}' )
    if [ "$TIME" != "00:00:00" ]; then
      TIME=$( date -d "$CURRENT_DATE $TIME" +%s ) #convert to epoch
    else
      NEXT_DATE=$(date +%Y-%m-%d -d "$CURRENT_DATE + 1 day")
      TIME=$( date -d "$NEXT_DATE $TIME" +%s ) #convert to epoch
      CURRENT_DATE=$NEXT_DATE
    fi
    parsedinfo="$TIME $TotalDISKREAD $TotalDISKWRITE $ProcDISKREAD $ProcDISKWRITE $SwapIn $IO"
    echo "$parsedinfo" >> $disklogfile
  done < $tmpfile
fi
rm $tmpfile
