#!/bin/bash

PID=$1
container_id=$2
storeddir=$3
loadlogfile=${storeddir}/${container_id}.loadlog
delay=1
default_OS='CentOS'

if [ -z $OS ]; then
  OS=$default_OS
fi

FULL_SETUP='0' #Check if user has sudo right and passwordless is setup
if [ $sudo_enabled = "yes" ]; then
  if [ $( sudo -n true | grep -c 'sudo: a password is required' ) = "0" ]; then #passwordless is setup
    FULL_SETUP='1'
  fi
fi

echo "TIME LOAD1M LOAD5M LOAD15M %US %SY %NI %ID %WA %HI %SI %ST MEMTOTAL MEMUSED MEMFREE BUFFERS SWAPTOTAL SWAPUSED SWAPFREE SWAPCACHED VIRT RES SHR S %CPU SWAP" > $loadlogfile
start_time=$( date +%s )
top -b -d $delay -p $PID > ${storeddir}/cpu_${PID}.log &
top_PID=$! #PID of top

while [ -e /proc/$PID ]; do #check if process still exist
  now=$( date +%s ) #time from epoch
  
  #get swap info of process
  SUMSWAP='0' # to indicate cannot read swap info for the process
  if [ "$FULL_SETUP" = "1" ]; then
    SUMSWAP=0
    for SWAP in `sudo grep Swap /proc/$PID/smaps 2>/dev/null| awk '{ print $2 }'`
    do
      let SUMSWAP=$SUMSWAP+$SWAP
    done
  fi
  
  # finalize the log
  echo "$SUMSWAP $now" >> ${storeddir}/swap_${PID}.log
  
  sleep $delay # period between snapshot = delay + script process time

done
# container has finished
kill $top_PID #kill top 

#TODO: format of 'uptime' can break the parsing process
##v1 top - 16:02:51 up 26 days,  6:14,
##v2 top - 05:15:07 up  5:22,
##v3 top - 05:15:07 up 1 day, 10 min,
# here is a temporary fix, but bugs may occur
if [ -e ${storeddir}/cpu_${PID}.log ]; then
  #add "0 day" to first line if the host has just started for less than 1 day.
  sed -i -e 's/^top .* load average/top - average/g' ${storeddir}/cpu_${PID}.log # remove the time, location - 8
#  sed -i -e '/up [0-9]* day/! s/ up / up 0 day, /' ${storeddir}/cpu_${PID}.log
#  sed -i -e '/[0-9]* min/! s/ min//' ${storeddir}/cpu_${PID}.log #remove min
fi

if [ -e ${storeddir}/cpu_${PID}.log ]; then
  sed -i -e ':a' -e 'N' -e '$!ba' -e 's/\n/ /g' ${storeddir}/cpu_${PID}.log
  sed -i -e "s/[[:space:]]\+/ /g" ${storeddir}/cpu_${PID}.log
  sed -i 's/,//g' ${storeddir}/cpu_${PID}.log
  sed -i 's/top - /\ntop - /g' ${storeddir}/cpu_${PID}.log
  sed -i '/^ *$/d' ${storeddir}/cpu_${PID}.log
#parsing info
  if [ "$OS" = "CentOS" ]; then
    while read line
    do
      info=$( echo "$line" | awk  '{print $4,$5,$6,$19,$20,$21,$22,$23,$24,$25,$26,$28,$30,$32,$34,$37,$39,$41,$43,$61,$62,$63,$64,$65}' )
      #prepare the output to have same format between top version of different OS
      #used centos format as the defaults
      L1M="$( echo "$info" | cut -d' ' -f1 )"
      L5M="$( echo "$info" | cut -d' ' -f2 )"
      L15M="$( echo "$info" | cut -d' ' -f3 )"
      #there are cases that one of the value below is 100, the space between them will disappear: 0.8%ni100.0%id
      if [ "$( echo "$info" | grep -c '\.[0-9]%[a-zA-Z][a-zA-Z]100\.0%[a-zA-Z][a-zA-Z]')" = "1" ]; then
        info="$( echo "$info" | sed 's/100.0%/ 100.0%/g' )" #separate them
      fi
      US="$( echo "$info" | cut -d' ' -f4 )"
      SY="$( echo "$info" | cut -d' ' -f5 )"
      NI="$( echo "$info" | cut -d' ' -f6 )"
      ID="$( echo "$info" | cut -d' ' -f7 )"
      WA="$( echo "$info" | cut -d' ' -f8 )"
      HI="$( echo "$info" | cut -d' ' -f9 )"
      SI="$( echo "$info" | cut -d' ' -f10 )"
      ST="$( echo "$info" | cut -d' ' -f11 )"
      #===============
      MTOT="$( echo "$info" | cut -d' ' -f12 )"
      MUSE="$( echo "$info" | cut -d' ' -f13 )"
      MFRE="$( echo "$info" | cut -d' ' -f14 )"
      MBUF="$( echo "$info" | cut -d' ' -f15 )"
      STOT="$( echo "$info" | cut -d' ' -f16 )"
      SUSE="$( echo "$info" | cut -d' ' -f17 )"
      SFRE="$( echo "$info" | cut -d' ' -f18 )"
      SCAC="$( echo "$info" | cut -d' ' -f19 )"
      VIRT="$( echo "$info" | cut -d' ' -f20 )"; if [ "$VIRT" = "" ]; then VIRT="0";fi
      RES="$( echo "$info" | cut -d' ' -f21 )"; if [ "$RES" = "" ]; then RES="0";fi
      SHR="$( echo "$info" | cut -d' ' -f22 )"; if [ "$SHR" = "" ]; then SHR="0";fi
      S="$( echo "$info" | cut -d' ' -f23 )"; if [ "$S" = "" ]; then S="Z";fi
      CPU="$( echo "$info" | cut -d' ' -f24 )"; if [ "$CPU" = "" ]; then CPU="0";fi
      swap="$( grep $start_time ${storeddir}/swap_${PID}.log | awk '{print $1}' )"; if [ "$swap" = "" ]; then swap="0";fi
      echo "$start_time $L1M $L5M $L15M $US $SY $NI $ID $WA $HI $SI $ST $MTOT $MUSE $MFRE $MBUF $STOT $SUSE $SFRE $SCAC $VIRT $RES $SHR $S $CPU $swap" >> $loadlogfile
      start_time=$(( $start_time + 1 ))
    done < ${storeddir}/cpu_${PID}.log
  elif [ "$OS" = "Ubuntu" ]; then
    while read line
    do
      #info=$( echo "$line" | awk  '{print $12,$13,$14,$27,$29,$31,$33,$35,$37,$39,$41,$45,$47,$49,$51,$55,$57,$59,$61,$80,$81,$82,$83,$84}' )
      info=$( echo "$line" | awk  '{print $4,$5,$6,$19,$21,$23,$25,$27,$29,$31,$33,$37,$39,$41,$43,$47,$49,$51,$53,$72,$73,$74,$75,$76}' )
      #prepare the output to have same format between top version of different OS
      L1M="$( echo "$info" | cut -d' ' -f1 )"
      L5M="$( echo "$info" | cut -d' ' -f2 )"
      L15M="$( echo "$info" | cut -d' ' -f3 )"
      US="$( echo "$info" | cut -d' ' -f4 )"
      SY="$( echo "$info" | cut -d' ' -f5 )"
      NI="$( echo "$info" | cut -d' ' -f6 )"
      ID="$( echo "$info" | cut -d' ' -f7 )"
      WA="$( echo "$info" | cut -d' ' -f8 )"
      HI="$( echo "$info" | cut -d' ' -f9 )"
      SI="$( echo "$info" | cut -d' ' -f10 )"
      ST="$( echo "$info" | cut -d' ' -f11 )"
      MTOT="$( echo "$info" | cut -d' ' -f12 )"
      MUSE="$( echo "$info" | cut -d' ' -f13 )"
      MFRE="$( echo "$info" | cut -d' ' -f14 )"
      MBUF="$( echo "$info" | cut -d' ' -f15 )"
      STOT="$( echo "$info" | cut -d' ' -f16 )"
      SUSE="$( echo "$info" | cut -d' ' -f17 )"
      SFRE="$( echo "$info" | cut -d' ' -f18 )"
      SCAC="$( echo "$info" | cut -d' ' -f19 )"
      VIRT="$( echo "$info" | cut -d' ' -f20 )"; if [ "$VIRT" = "" ]; then VIRT="0";fi
      RES="$( echo "$info" | cut -d' ' -f21 )"; if [ "$RES" = "" ]; then RES="0";fi
      SHR="$( echo "$info" | cut -d' ' -f22 )"; if [ "$SHR" = "" ]; then SHR="0";fi
      S="$( echo "$info" | cut -d' ' -f23 )"; if [ "$S" = "" ]; then S="Z";fi
      CPU="$( echo "$info" | cut -d' ' -f24 )"; if [ "$CPU" = "" ]; then CPU="0";fi
      swap="$( grep $start_time ${storeddir}/swap_${PID}.log | awk '{print $1}' )"; if [ "$swap" = "" ]; then swap="0";fi
      echo "$start_time $L1M $L5M $L15M $US%us $SY%sy $NI%ni $ID%id $WA%wa $HI%hi $SI%si $ST%st ${MTOT}k ${MUSE}k ${MFRE}k ${MBUF}k ${STOT}k ${SUSE}k ${SFRE}k ${SCAC}k $VIRT $RES $SHR $S $CPU $swap" >> $loadlogfile
      start_time=$(( $start_time + 1 ))
    done < ${storeddir}/cpu_${PID}.log
  else
    echo "Error, does not know OS"
  fi

  rm ${storeddir}/cpu_${PID}.log
  rm ${storeddir}/swap_${PID}.log
fi
