#!/bin/bash
locateddir="$usage_log_dir"
hostname="$observer"

PID=$1
container_id=$2
storeddir=$3
loadlogfile=${storeddir}/${container_id}.loadlog
disklogfile=${storeddir}/${container_id}.disklog
nodeinfofile=${storeddir}/${container_id}.nodeinfo
containerinfofile=${storeddir}/${container_id}.info
applog=$4

if [ "$metadata_enabled" = "" ]; then
  metadata_enabled="no" #default. set by environment variable.
fi

while [ -e /proc/$PID ]; do
  sleep 3
done

if [ -e ${loadlogfile} ]; then
  echo "scp -i $access_key ${loadlogfile} $user@$hostname:$locateddir" >> $applog
  scp -i $access_key ${loadlogfile} $user@$hostname:$locateddir 2>> $applog
  exitcode1=$?
  if [ $exitcode1 -eq 0 ]; then
    rm ${loadlogfile}
    echo "file ${loadlogfile} is sent to $hostname:$locateddir"
    echo "file ${loadlogfile} is sent to $hostname:$locateddir" >> $applog
  else
    echo "Cannot send ${loadlogfile} to $hostname"
    echo "Cannot send ${loadlogfile} to $hostname" >> $applog
  fi
else
  echo "file ${loadlogfile} is not found"
  echo "file ${loadlogfile} is not found" >> $applog
fi

if [ "$diskio" != "no"  ]; then
  if [ -e ${disklogfile} ]; then
    echo "scp -i $access_key ${disklogfile} $user@$hostname:$locateddir" >> $applog
    scp -i $access_key ${disklogfile} $user@$hostname:$locateddir 2>> $applog
    exitcode2=$?
    if [ $exitcode2 -eq 0 ]; then
      rm ${disklogfile}
      echo "file ${disklogfile} is sent to $hostname:$locateddir"
      echo "file ${disklogfile} is sent to $hostname:$locateddir" >> $applog
    else
      echo "Cannot send ${disklogfile} to $hostname"
      echo "Cannot send ${disklogfile} to $hostname" >> $applog
    fi
  else
    echo "file ${disklogfile} is not found"
    echo "file ${disklogfile} is not found" >> $applog
  fi
else
  if [ -e ${disklogfile} ]; then
    rm ${disklogfile}
    echo "Remove ${disklogfile} because the diskio feature is disable" >> $applog
  else
    echo "File ${disklogfile} is not created and no need for that file" >> $applog
  fi
fi

if [ "$metadata_enabled" = "no" ]; then
  if [ -e ${nodeinfofile} ]; then
    echo "scp -i $access_key ${nodeinfofile} $user@$hostname:$locateddir" >> $applog
    scp -i $access_key ${nodeinfofile} $user@$hostname:$locateddir 2>> $applog
    exitcode3=$?
    if [ $exitcode3 -eq 0 ]; then
      rm ${nodeinfofile}
      echo "file ${nodeinfofile} is sent to $hostname:$locateddir"
      echo "file ${nodeinfofile} is sent to $hostname:$locateddir" >> $applog
    else
      echo "Cannot send ${nodeinfofile} to $hostname"
      echo "Cannot send ${nodeinfofile} to $hostname" >> $applog
    fi
  else # does not have node info file, use the default one
    defaultsnodeinfofile=${storeddir}/defaults.nodeinfo
    cp ${defaultsnodeinfofile} ${nodeinfofile}
    if [ -e ${nodeinfofile} ]; then
      echo "Send defaults nodeinfo file ${nodeinfofile} to $hostname" >> $applog
      scp -i $access_key ${nodeinfofile} $user@$hostname:$locateddir 2>> $applog
      exitcode3=$?
      if [ $exitcode3 -eq 0 ]; then
        rm ${nodeinfofile}
        echo "default file ${nodeinfofile} is sent to $hostname:$locateddir"
        echo "default file ${nodeinfofile} is sent to $hostname:$locateddir" >> $applog
      else
        echo "Cannot send default file ${nodeinfofile} to $hostname"
        echo "Cannot send default file ${nodeinfofile} to $hostname" >> $applog
      fi
    else
      echo "file ${nodeinfofile} is still not found"
      echo "file ${nodeinfofile} is still not found" >> $applog
    fi
  fi

  if [ -e ${containerinfofile} ]; then
    echo "scp -i $access_key ${containerinfofile} $user@$hostname:$locateddir" >> $applog
    scp -i $access_key ${containerinfofile} $user@$hostname:$locateddir 2>> $applog
    exitcode4=$?
    if [ $exitcode4 -eq 0 ]; then
      rm ${containerinfofile}
      echo "file ${containerinfofile} is sent to $hostname:$locateddir"
      echo "file ${containerinfofile} is sent to $hostname:$locateddir" >> $applog
    else
      echo "Cannot send ${containerinfofile} to $hostname"
      echo "Cannot send ${containerinfofile} to $hostname" >> $applog
    fi
  else
    echo "file ${containerinfofile} is not found"
    echo "file ${containerinfofile} is not found" >> $applog
  fi
fi
