#!/bin/bash
container_id=$1
storeddir=$2

loadlogfile=${storeddir}/${container_id}.loadlog
disklogfile=${storeddir}/${container_id}.disklog
if [ "$metadata_enabled" = "no" ]; then
  nodeinfofile=${storeddir}/${container_id}.nodeinfo
  containerinfofile=${storeddir}/${container_id}.info
fi

applog=$3
timeout=10

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# Get container info first
if [ "$metadata_enabled" = "no" ]; then
  curl -s -XGET http://localhost:8042/ws/v1/node/containers/${container_id} > $containerinfofile &
fi
# Try to grep the PID correctly
# container need time to be initialized
numproc=$( ps ax | grep -c "[/]$container_id" )
echo "[process $$] numproc $numproc" >> $applog
# First case, process has not created
if [ $numproc -eq 0 ]; then #wait for first process in 10s
  try=1
  while [[ ( $numproc -eq 0 ) && ( $try -lt $timeout ) ]]; do
    sleep 1
    numproc=$( ps ax | grep -c "[/]$container_id" )
    let try=$try+1
    echo "[process $$] turn $try, try to wait for first process" >> $applog
  done
fi
# Second case, number of process is not enough 
while [[ ( $numproc -eq 1 ) || ($numproc -eq 2 ) ]]; do # at least one process should run already
  sleep 1
  numproc=$( ps ax | grep -c "[/]$container_id" )
  echo "[process $$] numproc $numproc" >> $applog
done
# Number of process is enought or the container has long gone
if [ $numproc -eq 3 ]; then #enough process 
#  try=1
#  containerPID=$( ps ax | grep "[/]$container_id" | tail -n 1 | awk '{print $1}' )
  containerproc=$( ps ax | grep "[/]$container_id" | tail -n 1 )
  containerPID=$( echo "$containerproc" | awk '{print $1}' )
### Usually we have correct container, however, there is cases that numproc reduce value before we grep PID
  numproc=$( ps ax | grep -c "[/]$container_id" ) #check the correctness of container PID
  if [ $numproc -ne 3 ]; then
    echo "[process $$] numproc reduce value to $numproc, clear PID for $container_id" >> $applog
    containerPID='' # remove the wrong value and give up
  fi
elif [ $numproc -gt 3 ]; then #more than enough
  echo "[process $$] numproc is weird, numproc value = $numproc, but we're okay with it" >> $applog
  ps ax | grep "[/]$container_id" >> $applog #for debug
  containerPID=$( ps ax | grep "[/]$container_id" | awk '{print $1}' | sed -n 3,3p )
else
  echo "[process $$] numproc is weird, numproc value = $numproc" >> $applog
  containerPID=''
fi

PID=$containerPID
if [ "$PID" = '' ]; then
  echo "container $container_id does not have PID with numproc = $numproc"
  echo "[process $$] container $container_id does not have PID because numproc = $numproc" >> $applog
  echo "[process $$] last command we know: $containerproc" >> $applog #for debug
  echo "[process $$] all processes at the time:" >> $applog #for debug
  ps ax | grep "[/]$container_id" >> $applog #for debug
else
  echo "[process $$] Container $container_id has PID of $PID" >> $applog
  if [ "$cpumem" = "yes" ]; then
    $DIR/trackMemoryCPUbyPID.sh $PID $container_id $storeddir &
    trackMCPID=$!
  fi
  if [ "$diskio" = "yes" ]; then
    $DIR/trackDiskIObyPID.sh $PID $container_id $storeddir &
    trackDIOPID=$!
  fi
  if [ "$networkio" = "yes" ]; then
    #TODO: track networkio
    echo "Call network I/O tracker for container $container_id"
  fi
  #wait for trackers finish
  if [ "$cpumem" = "yes" ]; then
    wait $trackDIOPID
    echo "loadlog of container $container_id is stored at $loadlogfile"
  fi
  if [ "$diskio" = "yes" ]; then
    wait $trackMCPID
    echo "disklog of container $container_id is stored at $disklogfile"
  fi
  if [ "$networkio" = "yes" ]; then
    #TODO: wait for tracker finish
    echo "disklog of container $container_id is stored at $netlogfile"
  fi
  #trackers finish
  #get node info, run on local host, no need to specify port
  if [ "$metadata_enabled" = "no" ]; then
    $DIR/getnodeinfo.sh $container_id $storeddir
    echo "nodeinfo of container $container_id is stored at $nodeinfofile"
    echo "info of container $container_id is stored at $containerinfofile"
  fi
  echo "[process $$] logs for resource usage of container $container_id is stored at $storeddir" >> $applog
fi
# notify tracking finish
