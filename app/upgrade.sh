#!/bin/bash

###### Upgrade software on cluster ########

###### Configure
curdir=`dirname "$0"`
DIR=`cd $curdir; pwd`
export confdir="${DIR}/conf"
. "${DIR}/conf/configure.sh"

##############################
pam1=$1
function usage {
  echo "Incorrect syntax, use:"
  echo "$( basename $0 ) --clean    -> upgrade software, and clean the old one"
  echo "$( basename $0 ) --no-clean -> keep temp file of old software, upgrade old software"
  echo "--clean is not working if output files and tmp. files are stored out of the executing folder"
  exit 1
}

if [ "$pam1" = "--clean" ]; then
  ${DIR}/tracker-manager.sh force-stop observer
  ${DIR}/tracker-manager.sh force-stop trackers
  ${DIR}/tracker-manager.sh clean observer
  ${DIR}/tracker-manager.sh clean trackers
  ${DIR}/tracker-manager.sh update-script observer
  ${DIR}/tracker-manager.sh update-script trackers
  ${DIR}/tracker-manager.sh start observer
  ${DIR}/tracker-manager.sh start trackers
elif [ "$pam1" = "--no-clean" ]; then
  ${DIR}/tracker-manager.sh force-stop observer
  ${DIR}/tracker-manager.sh force-stop trackers
  ${DIR}/tracker-manager.sh update-script observer
  ${DIR}/tracker-manager.sh update-script trackers
  ${DIR}/tracker-manager.sh start observer
  ${DIR}/tracker-manager.sh start trackers
else
  usage
fi
